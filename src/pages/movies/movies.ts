import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionService } from '../../services/QuestionService';
import { DomSanitizer } from '@angular/platform-browser';
import { RegisterPage } from "../register/register";
/**
 * Generated class for the MoviesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-movies',
  templateUrl: 'movies.html',
})
export class MoviesPage {
  public movies: any;
  public allMovies: any;
  public selected = 5;


  constructor(public navCtrl: NavController, public navParams: NavParams, public questionService: QuestionService, private sanitizer: DomSanitizer) {
    this.questionService.getAllQuestions('getQuestions').then((data: any) => {
      this.allMovies = data;
      this.changeStstus(this.selected)
    });
  }

  goToRegister() {
    this.navCtrl.push(RegisterPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoviesPage');
  }

  changeStstus(status) {
    this.selected = status;

    this.movies = this.allMovies.filter(item => {
      return item['answer1'] == this.selected;
    })
  }

}
