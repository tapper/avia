import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
import { TrainingsSevice } from "../../services/TraingngsService";
import { TrainingsPage } from "../trainings/trainings";


@IonicPage()
@Component({
  selector: 'page-add-training',
  templateUrl: 'add-training.html',
})
export class AddTrainingPage implements OnInit {

  TrainingsTypes = [];
  isDataAvailable: boolean = false;
  trainingsForm: FormGroup;
  time: string = ''; type: number = -1; date: string = '';

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams, public TrainingsService: TrainingsSevice) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTrainingPage');
  }

  ngOnInit() {
    this.TrainingsService.getTrainigTypes().then((data: any) => { this.TrainingsTypes = data, console.log("Types12 : ", this.TrainingsTypes), this.isDataAvailable = true; })
  }

  closePopUp() {
    this.viewCtrl.dismiss();
  }



  onSubmit(form: NgForm) {
    console.log(this.date, this.time, this.type)
    if (this.date == '')
      alert("חובה לבחור תאריך")
    else if (this.time == '')
      alert("חובה לבחור שעת אימון")
    else if (this.type < 0)
      alert("בחר סוג אימון")
    else {
      this.TrainingsService.addTraining("addTraining", form.value.time, form.value.date, form.value.type).then((data: any) => { console.log("DT2 : ", data); this.navCtrl.push(TrainingsPage) })
      form.reset();
      this.viewCtrl.dismiss();
    }
  }
}
