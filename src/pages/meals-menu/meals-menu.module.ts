import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MealsMenuPage } from './meals-menu';

@NgModule({
  declarations: [
    MealsMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(MealsMenuPage),
  ],
  exports: [
    MealsMenuPage
  ]
})
export class MealsMenuPageModule {}
