import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AddTrainingPage } from "../add-training/add-training";
import { TrainingsSevice } from "../../services/TraingngsService";
import { Config } from "../../services/config";
import { RegisterPage } from "../register/register";


/**
 * Generated class for the TrainingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-trainings',
    templateUrl: 'trainings.html',
})


export class TrainingsPage implements OnInit {

    TrainingsArray: { id: number, uid: number, date: string, time: string, type: number }[];
    TrainingsTypes: { id: number, title: string }[];
    isDataAvailable: boolean = false;


    constructor(public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, public TrainingsService: TrainingsSevice, public Settings: Config) {
        console.log("Tr1");
    }

    ionViewDidLoad() {
    }

    ngOnInit() {
        //this.TrainingsService.getTrainigTypes().then((data: any) => {this.TrainingsTypes  = data , console.log("Types : " , this.TrainingsTypes[0].title),this.isDataAvailable = true;})
        this.TrainingsService.getAllTrainings('getTraining').then((data: any) => {
            this.TrainingsArray = data.reverse(), console.log("All: ", this.TrainingsArray)
        })
        this.TrainingsTypes = this.TrainingsService.getTrainigTypesArray()
        //this.TrainingsArray = this.TrainingsService.getAllTrainingsArray()
        console.log("Config : ", this.TrainingsTypes)
    }

    gotoAddTraining() {
        // this.navCtrl.push(AddTrainingPage)

        const AddTraining = this.modalCtrl.create(AddTrainingPage, undefined, { cssClass: 'custom-modal' });
        AddTraining.onDidDismiss(data => {
            console.log(data);
        });

        AddTraining.present();
    }

    // getTrainingType(Id) {
    //     return this.TrainingsTypes[Id].title;
    // }

    goRegister() {
        this.navCtrl.push(RegisterPage)
    }


    changDateDirection(Date) {
        if (Date) {
            Date = Date.split('-')
            Date = Date[2] + '-' + Date[1] + '-' + Date[0];
            return Date;
        }

    }

    getTrainingType(type) {
        console.log("TP : ", type, this.TrainingsTypes)
        if (this.TrainingsTypes) {
            if (this.TrainingsTypes[type])
                return this.TrainingsTypes[type].title;
            else
                return "";
        } else
            return "";

    }
}

