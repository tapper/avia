import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomersPage } from './customers';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    CustomersPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomersPage),
    PipesModule
  ],
})
export class CustomersPageModule {


}
