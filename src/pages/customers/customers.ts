import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionService } from '../../services/QuestionService';
import { DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the CustomersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html',
})
export class CustomersPage {
  public movies: any;

  constructor(public navCtrl: NavController, private sanitizer: DomSanitizer, public navParams: NavParams, public questionService: QuestionService) {
    this.questionService.getAllQuestions('getQuestions').then((data: any) => {
      console.log("Movies : ", data)

      this.movies = data.filter(item => {
        console.log("It : ", item)
        return item['answer1'] == 3;
      })

      // this.movies = this.movies['title'];

      console.log("MoviesCustomers : ", this.movies)
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomersPage');
  }

}
