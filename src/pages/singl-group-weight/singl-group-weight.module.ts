import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SinglGroupWeightPage } from './singl-group-weight';

@NgModule({
  declarations: [
    SinglGroupWeightPage,
  ],
  imports: [
    IonicPageModule.forChild(SinglGroupWeightPage),
  ],
  exports: [
    SinglGroupWeightPage
  ]
})
export class SinglGroupWeightPageModule {}
