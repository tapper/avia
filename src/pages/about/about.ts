import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { QuestionService } from '../../services/QuestionService';
import { DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  public Contact = {
    'name': '',
    'mail': '',
    'details': '',
    'phone': '',
  }

  public movie: string = "";

  sendForm = true;
  constructor(public navCtrl: NavController, private toastCtrl: ToastController, public navParams: NavParams, public questionService: QuestionService, private sanitizer: DomSanitizer) {

    this.movie = questionService.QuestionsArray.find(item => {
      console.log("It : ", item)
      return item['answer1'] == 2;
    })

    this.movie = this.movie['title'];

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AboutPage');
  }

  // SendForm() {
  //   console.log("Sendform : ", this.sendForm)
  //   if (this.Contact.name == '')
  //     alert('אנא מלא שם מלא')
  //   else if (this.Contact.phone.length <= 7)
  //     alert('הכנס מספר טלפון תקין')
  //   else if (!this.ValidateEmail(this.Contact.mail))
  //     alert('הכנס מייל תקין בבקשה')
  //   else{

  //     this.sendForm = false;
  //   }
  // }

  SendForm() {

    if (this.Contact.name.length < 3)
      this.presentToast("יש למלא שם מלא", 0)

    else if (!this.ValidateEmail(this.Contact.mail))
      this.presentToast("יש למלא מייל", 0)

    else if (this.Contact.phone.length < 3)
      this.presentToast("יש למלא טלפון", 0)

    // else if (this.Contact.details.length < 3)
    //   this.presentToast("יש למלא תוכן הפנייה", 0)
    else {
      //this.Settings.ContactDetails = this.Contact;
      //this.Server.sendContactDetails('getContact');
      this.questionService.sendDetails('sendDetails', this.Contact, '000').then((data: any) => {
        console.log("Questions: ", data)

        this.presentToast("תודה נחזור אליך בהקדם", 1)
        this.Contact.name = '';
        this.Contact.mail = '';
        this.Contact.details = '';
        this.Contact.phone = '';

        this.sendForm = false;
      })
    }
  }


  presentToast(message, type) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
      cssClass: 'Toast'
    });

    toast.onDidDismiss(() => {

    });

    toast.present();
  }


  ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return (true)
    }
    return (false)
  }

}
