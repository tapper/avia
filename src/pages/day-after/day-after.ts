import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TrainingsPage} from "../trainings/trainings";

/**
 * Generated class for the DayAfterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-day-after',
  templateUrl: 'day-after.html',
})
export class DayAfterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DayAfterPage');
  }

  gotoWeightPage()
  {
      this.navCtrl.push(TrainingsPage)
  }

}
