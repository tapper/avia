import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { WeightService } from '../../services/weightService';
import { FormGroup, NgForm } from '@angular/forms';

/**
 * Generated class for the AddweightPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addweight',
  templateUrl: 'addweight.html',
})
export class AddweightPage {
  weight: number = 0; date: string = '';
  trainingsForm: FormGroup;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams, public weightService: WeightService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTrainingPage');
  }

  ngOnInit() {

  }

  closePopUp() {
    this.viewCtrl.dismiss();
  }



  onSubmit(form: NgForm) {
    console.log(this.date, this.weight)
    if (this.date == '')
      alert("חובה לבחור תאריך")
    else if (this.weight == 0)
      alert("חובה להזין משקל")
    else {
      this.weightService.addWeight("addWeight", this.date, this.weight).then((data: any) => { })
      form.reset();
      this.viewCtrl.dismiss();
    }
  }

}
