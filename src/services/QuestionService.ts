import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import { Config } from "./config";




@Injectable()

export class QuestionService {
    public QuestionsArray;
    public ServerUrl;

    constructor(private http: Http, public Settings: Config) {
        this.ServerUrl = this.Settings.ServerUrl;
        this.getAllQuestions('getQuestions')
    };

    getAllQuestions(url: string) {
        let body = new FormData();
        //body.append('uid', this.Settings.UserId.toString());
        return this.http.get(this.ServerUrl + '' + url).map(res => res.json()).do((data) => {
            this.QuestionsArray = data
            console.log("QuestionsArray : ", this.QuestionsArray)
        }).toPromise();
    }

    getQuestionsArray() {
        return this.QuestionsArray;
    }

    sendDetails(url: string, info, score) {

        let body = '&selected=' + JSON.stringify(info) + '&score=' + score;
        console.log("SAVVVE ", info)
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res).do((data) => { console.log("Question : ", data) }).toPromise();
    }
};


