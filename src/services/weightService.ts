
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import { Config } from "./config";



@Injectable()

export class WeightService {
    public WeightArray;
    public ServerUrl;

    constructor(private http: Http, public Settings: Config) {
        this.ServerUrl = this.Settings.ServerUrl;
    };

    getAllWeightByUserid(url: string) {
        let body = new FormData();
        //body.append('uid', this.Settings.CourseId.toString());
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data) => { this.WeightArray = data }).toPromise();
    }

    getAllWeightArray() {
        console.log("AAA : ", this.WeightArray)
        return this.WeightArray;
    }

    checkWeight(url) {
        let body = new FormData();
        //body.append('uid', this.Settings.CourseId.toString());
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data) => { }).toPromise();
    }

    addWeight(url: string, date: string, weight: number) {
        let body = new FormData();
        body.append('date', date);
        body.append('weight', String(weight));
        body.append('uid', this.Settings.UserId.toString());
        body.append('cid', localStorage.course_id);
        //addWeight
        console.log("Body ", url, date, weight, this.Settings);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data) => { }).toPromise();
    }

};


