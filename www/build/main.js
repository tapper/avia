webpackJsonp([18],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(333);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










//constructor(@Inject(DOCUMENT) private document: Document) { }
/**
 * Generated class for the ChatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ChatPage = /** @class */ (function () {
    function ChatPage(navCtrl, element, loadingCtrl, zone, navParams, ChatService, platform, Settings, events, actionSheet, camera, file, transfer, filePath, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.element = element;
        this.loadingCtrl = loadingCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.ChatService = ChatService;
        this.platform = platform;
        this.Settings = Settings;
        this.events = events;
        this.actionSheet = actionSheet;
        this.camera = camera;
        this.file = file;
        this.transfer = transfer;
        this.filePath = filePath;
        this.modalCtrl = modalCtrl;
        this.chatText = '';
        this.ChatArray = [];
        this.screenHeight = screen.height - 100 + 'px';
        this.rowNum = 1;
        this.lastImage = null;
        this.ChatService._ChatArray.subscribe(function (val) {
            _this.zone.run(function () {
                _this.ChatArray = val;
            });
        });
        this.phpHost = Settings.phpHost;
        this.ServerUrl = Settings.ServerUrl;
        document.addEventListener('resume', function () {
            _this.getChatDetails();
        });
        if (typeof this.loading != "undefined")
            this.loading.dismiss();
        this.loading = this.loadingCtrl.create({
            content: 'עוד רגע מתחילים'
        });
        events.subscribe('newchat', function (user, time) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.scrollBottom();
        });
        //this.addTextArea()
    }
    ChatPage.prototype.ngOnInit = function () {
        this.getChatDetails();
        this.scrollBottom();
        this.innerHeight = (window.screen.height);
        var elm = document.querySelector(".chatContent");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.23)) + 'px';
    };
    ChatPage.prototype.addTextArea = function () {
        this.TextAreaContent = document.getElementById('TextContent');
        console.log("TA : " + this.TextAreaContent);
        this.TextAreaContent.innerHTML = '<ion-textarea class="chatInput" id="chatInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)"></ion-textarea>';
    };
    ChatPage.prototype.scrollBottom = function () {
        var _this = this;
        setTimeout(function () {
            //var objDiv = <HTMLElement>document.querySelector(".chatContent");
            //objDiv.scrollTop = objDiv.scrollHeight;
            var element = document.getElementById("chatContent");
            element.scrollIntoView(true);
            element.scrollTop = element.scrollHeight;
            _this.content.scrollToBottom(0);
        }, 300);
    };
    ChatPage.prototype.scrollToBottom = function () {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
        catch (err) { }
    };
    ChatPage.prototype.getChatDetails = function () {
        //this.loading.present();
        var _this = this;
        this.ChatService.getChatDetails('getChatDetailsForTest').then(function (data) {
            //this.loading.dismiss();
            console.log("ChatDetails: ", data), _this.ChatArray = data.reverse(), _this.scrollBottom();
        });
    };
    ChatPage.prototype.ionViewDidLoad = function () {
        this.scrollBottom();
        console.log("TP : ", this.loading);
        //if(typeof this.loading != "undefined")
        //this.loading.dismiss();
    };
    ChatPage.prototype.ionViewWillEnter = function () {
        this.scrollBottom();
        //if(typeof this.loading != "undefined")
        //this.loading.dismiss();
    };
    ChatPage.prototype.addChatTitle = function (isImage) {
        var _this = this;
        var chatText1 = '';
        var chatType = '';
        if (isImage == 1) {
            chatText1 = this.uploadedImagePath;
            chatType = '3';
        }
        else {
            chatText1 = this.chatText;
            chatType = '1';
        }
        if (chatText1) {
            this.chatText = '';
            this.date = new Date();
            this.hours = this.date.getHours();
            this.minutes = this.date.getMinutes();
            this.seconds = this.date.getSeconds();
            if (this.hours < 10)
                this.hours = "0" + this.hours;
            if (this.minutes < 10)
                this.minutes = "0" + this.minutes;
            this.time = this.hours + ':' + this.minutes;
            this.today = new Date();
            this.dd = this.today.getDate();
            this.mm = this.today.getMonth() + 1; //January is 0!
            this.yyyy = this.today.getFullYear();
            if (this.dd < 10) {
                this.dd = '0' + this.dd;
            }
            if (this.mm < 10) {
                this.mm = '0' + this.mm;
            }
            this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
            this.newdate = this.today + ' ' + this.time;
            this.Obj = {
                id: '',
                uid: window.localStorage.identify,
                name: window.localStorage.name,
                title: chatText1,
                date: this.newdate,
                type: chatType,
                image: window.localStorage.user_image
            };
            this.ChatService.pushToArray(this.Obj);
            this.ChatService.addTitle('addChatTitle', chatText1, this.newdate, window.localStorage.name, window.localStorage.user_image, chatType).then(function (data) {
                console.log("Weights : ", data), chatText1 = '', _this.scrollBottom(), _this.uploadedImagePath = '';
            });
            this.autoSize(40);
            //let txtArea = <HTMLElement>document.querySelector(".TextContent")
            //txtArea.innerHTML = '<ion-textarea class="chatInput" id="myInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)" #myInput></ion-textarea>';
            //txtArea.style.height = "auto";
        }
    };
    ChatPage.prototype.enlargeImage = function (newimage) {
        /*
        let modalObj = {imagePath: newimage };
        let ChatImageModal = this.modalCtrl.create(ChatModalPage, modalObj);
        ChatImageModal.present();
        */
    };
    // autoSize(pixels)
    // {
    //     let textArea = this.element.nativeElement.getElementsByTagName('title')[0];
    //     console.log(textArea)
    //     textArea.style.overflow = 'hidden';
    //     textArea.style.height  = 'auto';
    //     if (pixels === 0){
    //         textArea.style.height  = textArea.scrollHeight + 'px';
    //     } else {
    //         textArea.style.height  = pixels + 'px';
    //     }
    //     return;
    // }
    ChatPage.prototype.photoOptions = function () {
        var _this = this;
        var actionSheet = this.actionSheet.create({
            title: 'בחירת מקור התמונה',
            buttons: [
                {
                    text: 'גלריית תמונות',
                    icon: 'albums',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'מצלמה',
                    icon: 'camera',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ChatPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    ChatPage.prototype.uploadPhoto = function () {
        var _this = this;
        // Destination URL
        //var url = "http://tapper.org.il/647/laravel/public/api/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'טוען...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, this.ServerUrl + 'uploadImage', options).then(function (data) {
            console.log("Updata  : ", data.response);
            //this.serverImage = data.response;
            _this.loading.dismissAll();
            _this.uploadedImagePath = data.response;
            _this.addChatTitle(1);
        }, function (err) {
            _this.loading.dismissAll();
        });
    };
    ChatPage.prototype.takePhoto = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            console.log("f0");
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                console.log("f1");
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    _this.correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    _this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                });
            }
            else {
                console.log("f1");
                _this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                console.log("f2 : ", _this.currentName);
                _this.correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                console.log("f3 : ", _this.correctPath);
                _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                console.log("f4");
            }
        }, function (err) {
            //this.presentToast('Error while selecting image.');
        });
    };
    ChatPage.prototype.createFileName = function () {
        console.log("f1");
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    ChatPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("f2 : " + namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.uploadPhoto();
        }, function (error) {
            //this.presentToast('Error while storing file.');
        });
    };
    ChatPage.prototype.autoSize = function (pixels) {
        if (pixels === void 0) { pixels = 0; }
        // let textArea = this.element.nativeElement.getElementsByTagName('title')[0];
        var textArea = document.querySelector(".chatInput");
        console.log(textArea);
        textArea.style.overflow = 'hidden';
        textArea.style.height = 'auto';
        if (pixels === 0) {
            textArea.style.height = textArea.scrollHeight + 'px';
        }
        else {
            textArea.style.height = pixels + 'px';
        }
        return;
    };
    ChatPage.prototype.getHour = function (DateStr) {
        var Hour = DateStr.split(" ");
        var Hour1 = String(Hour[0]).split("/");
        return Hour1[0] + "/" + Hour1[1] + " | " + Hour[1];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
    ], ChatPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('scrollMe'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ChatPage.prototype, "myScrollContainer", void 0);
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\chat\chat.html"*/'<!--\n\n  Generated template for the TrainingsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n    <ion-toolbar no-border-top class="ToolBarClass">\n\n\n\n            <div class="ToolBarBackButton">\n\n                <button  ion-button icon-only clear navPop>\n\n                    <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n                </button>\n\n            </div>\n\n\n\n        <div class="ToolBarTitle">\n\n            <ion-title>\n\n                <img src="images/head_logo.png" class="headLogo"  />\n\n            </ion-title>\n\n        </div>\n\n        <ion-buttons end>\n\n            <button (click)="photoOptions()" ion-button icon-only>\n\n                <ion-icon name="md-camera"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="ContentClass">\n\n\n\n\n\n    <div class="chatContent" id="chatContent" #scrollMe [scrollTop]="scrollMe.scrollHeight" style="padding-bottom: 70px;">\n\n\n\n            <div class="chatMessage" id="chatMessage" *ngFor="let message of ChatArray">\n\n                <div class="chatMessageRight">\n\n                    <img *ngIf="message.image" src="{{phpHost+message.image}}" class="imageclass"/>\n\n                    <img *ngIf="!message.image"  src="images/647_chat.jpg" class="imageclass"/>\n\n                </div>\n\n                <div [ngClass]="{\'chatMessageLeft\' : message.type != 2, \'chatMessageLeftBlue\':message.type == 2}">\n\n                    <div [ngClass]="{\'chatTitle\' : message.type != 2, \'chatTitleBlue\':message.type == 2}" >\n\n                        <div [ngClass]="{\'chatTitleLeft\' : message.type != 2, \'chatTitleLeftBlue\':message.type == 2}" >\n\n                            <p>{{getHour(message.date)}}</p>\n\n                        </div>\n\n                        <div [ngClass]="{\'chatTitleRight\' : message.type != 2, \'chatTitleRightBlue\':message.type == 2}" >\n\n                            <p *ngIf="message.type != 2">{{message.name}}</p>\n\n                            <p *ngIf="message.type == 2"> {{message.name}} דווח ארוחה !!</p>\n\n                        </div>\n\n                    </div>\n\n                    <p class="chatMessageLeftP" *ngIf="message.type != 3">{{message.title}}</p>\n\n                    <p class="chatMessageLeftP" *ngIf="message.type == 3" (click)="enlargeImage(message.title)">\n\n                        <img *ngIf="message.title" src="{{phpHost+message.title}}" class="photoImage2" imageViewer />\n\n                    </p>\n\n                </div>\n\n\n\n            </div>\n\n    </div>\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n<ion-footer class="footerClass">\n\n    <div class="chatFooterDiv" align="center">\n\n        <div class="chatFooterDivRight">\n\n            <ion-item align="center" class="TextContent">\n\n                <!--<ion-textarea (input)="autoSize()" class="chatInput" id="myInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)"></ion-textarea>-->\n\n                <ion-textarea (input)="autoSize();"  class="chatInput" rows="1"  autosize placeholder="הוסף הודעה" [(ngModel)]="chatText"></ion-textarea>\n\n            </ion-item>\n\n        </div>\n\n        <div class="chatFooterDivLeft" (click)="addChatTitle(0)">\n\n            <img src="images/addchat.png" class="imageclass"/>\n\n        </div>\n\n\n\n    </div>\n\n\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\chat\chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_chatService__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_QuestionService__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, toastCtrl, navParams, questionService, sanitizer) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.questionService = questionService;
        this.sanitizer = sanitizer;
        this.Contact = {
            'name': '',
            'mail': '',
            'details': '',
            'phone': '',
        };
        this.movie = "";
        this.sendForm = true;
        this.movie = questionService.QuestionsArray.find(function (item) {
            console.log("It : ", item);
            return item['answer1'] == 2;
        });
        this.movie = this.movie['title'];
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad AboutPage');
    };
    // SendForm() {
    //   console.log("Sendform : ", this.sendForm)
    //   if (this.Contact.name == '')
    //     alert('אנא מלא שם מלא')
    //   else if (this.Contact.phone.length <= 7)
    //     alert('הכנס מספר טלפון תקין')
    //   else if (!this.ValidateEmail(this.Contact.mail))
    //     alert('הכנס מייל תקין בבקשה')
    //   else{
    //     this.sendForm = false;
    //   }
    // }
    AboutPage.prototype.SendForm = function () {
        var _this = this;
        if (this.Contact.name.length < 3)
            this.presentToast("יש למלא שם מלא", 0);
        else if (!this.ValidateEmail(this.Contact.mail))
            this.presentToast("יש למלא מייל", 0);
        else if (this.Contact.phone.length < 3)
            this.presentToast("יש למלא טלפון", 0);
        else {
            //this.Settings.ContactDetails = this.Contact;
            //this.Server.sendContactDetails('getContact');
            this.questionService.sendDetails('sendDetails', this.Contact, '000').then(function (data) {
                console.log("Questions: ", data);
                _this.presentToast("תודה נחזור אליך בהקדם", 1);
                _this.Contact.name = '';
                _this.Contact.mail = '';
                _this.Contact.details = '';
                _this.Contact.phone = '';
                _this.sendForm = false;
            });
        }
    };
    AboutPage.prototype.presentToast = function (message, type) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            cssClass: 'Toast'
        });
        toast.onDidDismiss(function () {
        });
        toast.present();
    };
    AboutPage.prototype.ValidateEmail = function (mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true);
        }
        return (false);
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\about\about.html"*/'<ion-header>\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button ion-button icon-only clear navPop>\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo" />\n\n      </ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <!-- <div class="AboutInfo">\n\n    <img src="images/home/1.jpg" style="width:100%" />\n\n  </div> -->\n\n  <div class="backgroundImage">\n\n    <img src="images/trainings_bck.jpg" class="imageclass" />\n\n  </div>\n\n\n\n\n\n  <div class="loginMain333 AboutInfo" *ngIf="sendForm">\n\n    <div class="loginTitle">\n\n      איך נפסיק לנשנש ב 3 ימים?\n\n    </div>\n\n    <div class="loginDesc">\n\n      לצפייה בסרטון יש למלא את הפרטים\n\n    </div>\n\n    <div class="InputsText mt-5" align="center">\n\n      <div class="InputText">\n\n        <input type="text" placeholder=" שם מלא" [(ngModel)]="Contact.name" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder=" מספר טלפון" [(ngModel)]="Contact.phone" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder=" אימייל" [(ngModel)]="Contact.mail" />\n\n      </div>\n\n      <!-- <div class="InputText mt10">\n\n        <textarea [(ngModel)]="Contact.details" name="info" placeholder=" קצת פרטים עלייך" rows="4"></textarea>\n\n      </div> -->\n\n\n\n      <button ion-button class="IntroButton" (click)="SendForm()">שלח פרטים</button>\n\n    </div>\n\n  </div>\n\n\n\n  <div class="loginMain AboutInfo VideoInfo" *ngIf="!sendForm">\n\n    <!-- <iframe width="100%" height="315"\n\n      [src]="sanitizer.bypassSecurityTrustResourceUrl(\'https://www.youtube.com/embed/\'+movie)" frameborder="0"\n\n      allowfullscreen></iframe> -->\n\n\n\n      <iframe class="youTubeVideo" [src]="\'https://www.youtube.com/embed/\'+movie | safe" frameborder="0" allowfullscreen\n\n      width="100%" height="315"></iframe>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\about\about.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_QuestionService__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddTrainingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_TraingngsService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trainings_trainings__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddTrainingPage = /** @class */ (function () {
    function AddTrainingPage(navCtrl, viewCtrl, navParams, TrainingsService) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.TrainingsService = TrainingsService;
        this.TrainingsTypes = [];
        this.isDataAvailable = false;
        this.time = '';
        this.type = -1;
        this.date = '';
    }
    AddTrainingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddTrainingPage');
    };
    AddTrainingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.TrainingsService.getTrainigTypes().then(function (data) { _this.TrainingsTypes = data, console.log("Types12 : ", _this.TrainingsTypes), _this.isDataAvailable = true; });
    };
    AddTrainingPage.prototype.closePopUp = function () {
        this.viewCtrl.dismiss();
    };
    AddTrainingPage.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(this.date, this.time, this.type);
        if (this.date == '')
            alert("חובה לבחור תאריך");
        else if (this.time == '')
            alert("חובה לבחור שעת אימון");
        else if (this.type < 0)
            alert("בחר סוג אימון");
        else {
            this.TrainingsService.addTraining("addTraining", form.value.time, form.value.date, form.value.type).then(function (data) { console.log("DT2 : ", data); _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__trainings_trainings__["a" /* TrainingsPage */]); });
            form.reset();
            this.viewCtrl.dismiss();
        }
    };
    AddTrainingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-training',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\add-training\add-training.html"*/'<!--\n\n  Generated template for the AddTrainingPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!-- <ion-header>\n\n\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button  ion-button icon-only clear navPop>\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo"  />\n\n      </ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n\n\n</ion-header> -->\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <!-- <div class="backgroundImage">\n\n    <img src="images/add_training_bck.png" class="imageclass" />\n\n  </div> -->\n\n\n\n  <div class="trainingText">\n\n    <div class="add_new_Training">\n\n      דווח אימון חדש\n\n    </div>\n\n\n\n    <form #f="ngForm" class="Form" (ngSubmit)="onSubmit(f)">\n\n      <ion-list>\n\n\n\n        <ion-item class="formElement">\n\n          <ion-label>בחר תאריך</ion-label>\n\n          <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="date" name="date"></ion-datetime>\n\n        </ion-item>\n\n\n\n        <ion-item class="formElement">\n\n          <ion-label>בחר שעה</ion-label>\n\n          <ion-datetime displayFormat="HH:mm " pickerFormat="HH mm " [(ngModel)]="time" name="time"></ion-datetime>\n\n        </ion-item>\n\n\n\n        <ion-item class="formElement" *ngIf="isDataAvailable">\n\n          <ion-label>בחר סוג אימון</ion-label>\n\n          <ion-select [(ngModel)]="type" name="type">\n\n            <ion-option *ngFor="let option of TrainingsTypes let i=index" [value]=i>{{option.title}}</ion-option>\n\n          </ion-select>\n\n        </ion-item>\n\n\n\n      </ion-list>\n\n\n\n      <button type="submit" ion-button block>דווח </button>\n\n      <a type="button" class="cancelBTN" ion-button block (click)="closePopUp()">בטל </a>\n\n    </form>\n\n  </div>\n\n\n\n  <!--\n\n  <div class="footerImage">\n\n    <img src="images/training_footer.png" class="imageclass"/>\n\n  </div>\n\n  -->\n\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\add-training\add-training.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_TraingngsService__["a" /* TrainingsSevice */]])
    ], AddTrainingPage);
    return AddTrainingPage;
}());

//# sourceMappingURL=add-training.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeightPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_weightService__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__addweight_addweight__ = __webpack_require__(173);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the WeightPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var WeightPage = /** @class */ (function () {
    function WeightPage(navCtrl, modalCtrl, navParams, WeightService, Settings) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.WeightService = WeightService;
        this.Settings = Settings;
        this.TabSelected = 1;
        this.GroupWeightOpen = 0;
        this.isDataAvailable = false;
        this.Uid = this.Settings.UserId;
    }
    WeightPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WeightPage');
    };
    WeightPage.prototype.ngOnInit = function () {
        var _this = this;
        // this.WeightArray = this.WeightService.getAllWeightArray();
        // this.WeightArray.sort();
        this.WeightService.getAllWeightByUserid('getWeight').then(function (data) { _this.WeightArray = data.reverse(), console.log("Weights : ", _this.WeightArray), _this.isDataAvailable = true; });
    };
    WeightPage.prototype.changeTab = function (Num) {
        this.TabSelected = Num;
        if (Num == 0)
            this.GroupWeightOpen = 0;
    };
    WeightPage.prototype.AddWeight = function () {
        var _this = this;
        var AddWeight = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__addweight_addweight__["a" /* AddweightPage */], undefined, { cssClass: 'custom-modal' });
        AddWeight.onDidDismiss(function (data) {
            _this.WeightService.getAllWeightByUserid('getWeight').then(function (data) { _this.WeightArray = data.reverse(), console.log("Weights : ", _this.WeightArray), _this.isDataAvailable = true; });
        });
        AddWeight.present();
    };
    WeightPage.prototype.openGroupPage = function (j) {
        this.GroupWeightOpen = 1;
        this.currentDateWeightArray = this.WeightArray[j].weights;
        console.log("01 : ", this.currentDateWeightArray);
        this.currentDateWeightArray.sort(function (a, b) {
            if (a.weight1 < b.weight1)
                return -1;
            else if (a.weight1 > b.weight1)
                return 1;
            else
                return 0;
        });
        console.log("02 : ", this.currentDateWeightArray);
    };
    WeightPage.prototype.changDateDirection = function (Date) {
        if (Date) {
            Date = Date.split('-');
            Date = Date[2] + '-' + Date[1] + '-' + Date[0];
            return Date;
        }
    };
    WeightPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-weight',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\weight\weight.html"*/'<ion-header>\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button ion-button icon-only clear navPop>\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo" />\n\n      </ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="Content">\n\n\n\n  <div style="width: 100%; margin-top: 15px;" align="center" (click)="AddWeight()">\n\n    <div style="width: 95%" align="center">\n\n      <button ion-button block padding style="font-size: 22px; font-weight: bold">הוסף שקילה</button>\n\n    </div>\n\n  </div>\n\n\n\n  <!-- Tabs -->\n\n  <div class="weightContent">\n\n    <div class="trainingsTitle">\n\n      טבלת שקילות\n\n    </div>\n\n\n\n    <div class="TraningInfo" *ngFor="let weight of WeightArray">\n\n      <div class="leftTraningInfo1">\n\n        תאריך : {{changDateDirection(weight.date)}}\n\n      </div>\n\n      <div class="rightTraningInfo1">\n\n        משקל : {{ weight.weight }}\n\n\n\n        <!--TrainingsTypes[training.type].title-->\n\n      </div>\n\n      <hr />\n\n    </div>\n\n\n\n    <!-- <div class="TabsButton" align="center">\n\n        <div class="TabsButtonDiv">\n\n          <div class="TabsButtonLeft">\n\n            <button ion-button [ngClass]="{\'PersonalSelected\' : (TabSelected == 0), \'GroupSelected\':(TabSelected == 1)}" (click)="changeTab(0)" block>הקבוצה שלי </button>\n\n          </div>\n\n          <div class="TabsButtonRight">\n\n            <button ion-button [ngClass]="{\'PersonalSelected\' : (TabSelected == 1), \'GroupSelected\':(TabSelected == 0)}" (click)="changeTab(1)" full>המשקל שלי</button>\n\n          </div>\n\n        </div>\n\n    </div> -->\n\n\n\n    <!-- Personal Weight -->\n\n    <!-- <div *ngIf="TabSelected == 1">\n\n      <div *ngFor="let weight of WeightArray let i=index">\n\n        <ion-list class="weightList" *ngFor="let w of WeightArray[i].weights">\n\n          <div *ngIf="Uid == w.attendee_id">\n\n            <ion-item class="weightItem">\n\n              <ion-label class="LabelRight">\n\n                {{w.weight}} <span class="KG">ק"ג</span>\n\n              </ion-label>\n\n              <ion-label class="LabelRight">\n\n                {{weight.date}}\n\n              </ion-label>\n\n            </ion-item>\n\n          </div>\n\n        </ion-list>\n\n      </div>\n\n    </div> -->\n\n\n\n\n\n    <!-- Group Dates -->\n\n\n\n    <!-- <div *ngIf="TabSelected == 0 && GroupWeightOpen == 0">\n\n      <div *ngFor="let weight of WeightArray let i=index">\n\n        <ion-list class="weightList" *ngFor="let w of weight.weights " (click)="openGroupPage(i)">\n\n          <div>\n\n            <!-- <ion-item class="weightItem" *ngIf="Uid == w.attendee_id">\n\n              <ion-label class="LabelLeft">\n\n                <ion-icon name="ios-arrow-back"></ion-icon>\n\n              </ion-label>\n\n              <ion-label class="LabelRight">\n\n                {{weight.date}}\n\n              </ion-label>\n\n            </ion-item> \n\n\n\n            <div class="leftTraningInfo1">\n\n              {{weight.date}}\n\n            </div>\n\n            <div class="rightTraningInfo1">\n\n              {{weight.weight}}\n\n              <!--TrainingsTypes[training.type].title\n\n            </div>\n\n            <hr />\n\n          </div>\n\n\n\n      </div>\n\n      </ion-list>\n\n    </div>\n\n  </div> -->\n\n\n\n    <!-- Group Weights -->\n\n    <!-- <div *ngIf="TabSelected == 0 && GroupWeightOpen == 1">\n\n    <ion-list class="weightList" *ngFor="let weight of currentDateWeightArray let i=index">\n\n      <div>\n\n        <ion-item class="weightItem">\n\n          <ion-label class="LabelLeft" style="direction: rtl;">\n\n            {{weight.weight1|number:\'1.2-2\'}} <span class="KG">ק"ג</span>\n\n          </ion-label>\n\n          <ion-label class="LabelRight">\n\n            {{weight.name}}\n\n          </ion-label>\n\n        </ion-item>\n\n      </div>\n\n    </ion-list>\n\n  </div> -->\n\n  </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\weight\weight.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_weightService__["a" /* WeightService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
    ], WeightPage);
    return WeightPage;
}());

//# sourceMappingURL=weight.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddweightPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_weightService__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddweightPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddweightPage = /** @class */ (function () {
    function AddweightPage(navCtrl, viewCtrl, navParams, weightService) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.weightService = weightService;
        this.weight = 0;
        this.date = '';
    }
    AddweightPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddTrainingPage');
    };
    AddweightPage.prototype.ngOnInit = function () {
    };
    AddweightPage.prototype.closePopUp = function () {
        this.viewCtrl.dismiss();
    };
    AddweightPage.prototype.onSubmit = function (form) {
        console.log(this.date, this.weight);
        if (this.date == '')
            alert("חובה לבחור תאריך");
        else if (this.weight == 0)
            alert("חובה להזין משקל");
        else {
            this.weightService.addWeight("addWeight", this.date, this.weight).then(function (data) { });
            form.reset();
            this.viewCtrl.dismiss();
        }
    };
    AddweightPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-addweight',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\addweight\addweight.html"*/'<ion-content padding>\n\n  <div class="trainingText">\n    <div class="add_new_Training">\n      הוספת שקילה חדשה\n    </div>\n\n    <form #f="ngForm" class="Form" (ngSubmit)="onSubmit(f)">\n      <ion-list>\n\n        <ion-item class="formElement">\n          <ion-label>בחר תאריך</ion-label>\n          <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="date" name="date"></ion-datetime>\n        </ion-item>\n\n        <ion-item class="formElement">\n          <ion-label>הוסף משקל</ion-label>\n          <ion-input [(ngModel)]="weight" name="weight"></ion-input>\n        </ion-item>\n\n\n\n      </ion-list>\n\n      <button type="submit" ion-button block>דווח </button>\n      <a type="button" class="cancelBTN" ion-button block (click)="closePopUp()">בטל </a>\n    </form>\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\addweight\addweight.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_weightService__["a" /* WeightService */]])
    ], AddweightPage);
    return AddweightPage;
}());

//# sourceMappingURL=addweight.js.map

/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MealsMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_MealsService__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__meals_meals__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var MealsMenuPage = /** @class */ (function () {
    function MealsMenuPage(navCtrl, navParams, MealsService, alertCtrl, zone) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.MealsService = MealsService;
        this.alertCtrl = alertCtrl;
        this.zone = zone;
        this.MealsTypesArray = [];
        this.MealsArray = [];
        this.MealsArray1 = [];
        this.MenuLimits = [];
        this.isDataAvailable1 = false;
        this.isDataAvailable2 = false;
        this.currentTab = 0;
        this.tabNum = 0;
        this.currentLimit = 0;
        this.currentSelected = 0;
        this.mealName = '';
        this.mealNum = navParams.get('mealNum');
        this.MealsService._MealsArray.subscribe(function (val) {
            _this.zone.run(function () {
                if (val) {
                    console.log("Contructor : ", val);
                    _this.MealsArray = val;
                    _this.items = _this.filterItems(_this.mealName);
                }
            });
        });
        this.MealsService._MenuLimits.subscribe(function (val) {
            _this.zone.run(function () {
                if (val) {
                    _this.MenuLimits = val; //this.MealsService.MenuLimits;
                    //this.currentLimit =  this.MealsService.MenuLimits[this.mealNum][0].qnt;
                    console.log("MenuLimits", _this.MenuLimits, val);
                    _this.isDataAvailable1 = true;
                }
            });
        });
        this.MealsService._MealsTypesArray.subscribe(function (val) {
            _this.zone.run(function () {
                if (val) {
                    _this.MealsTypesArray = _this.MealsService.getMealsTypesArray();
                    console.log("MealsTypesArray : ", _this.MealsTypesArray);
                    _this.isDataAvailable2 = true;
                }
            });
        });
    }
    MealsMenuPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad MealsMenuPage');
    };
    MealsMenuPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var elm;
            return __generator(this, function (_a) {
                //console.log("MenuLimits",this.MealsService.MenuLimits);
                //this.currentLimit = await this.MealsService.MenuLimits[this.mealNum][0].qnt;
                //this.MealsTypesArray = await this.MealsService.getMealsTypesArray();
                //console.log("MealsTypesArray",this.MealsTypesArray)
                //this.MealsArray = await this.MealsService.getMealsArray();
                //this.items =  this.filterItems(this.mealName);
                //this.currentTab = this.MealsTypesArray[0].id;
                this.innerHeight = (window.screen.height);
                elm = document.querySelector(".Question");
                elm.style.height = (this.innerHeight - (this.innerHeight * 0.4)) + 'px';
                return [2 /*return*/];
            });
        });
    };
    MealsMenuPage.prototype.changeTab = function (Id, tabNum) {
        console.log("changeTab : ", Id, tabNum, this.mealName);
        this.tabNum = tabNum;
        this.currentTab = Id;
        //this.currentLimit = this.MealsService.MenuLimits[this.mealNum][tabNum].qnt;
        this.items = this.filterItems(this.mealName);
        console.log("Items : ", this.items);
        this.mealName = "";
    };
    MealsMenuPage.prototype.itemClicked = function (MealsNumber, ItemId, index) {
        console.log("this.MealsArray[this.tabNum] : ", ItemId, index, this.MealsArray[this.tabNum]);
        if (this.MealsArray[this.tabNum][index].selected[this.mealNum].selected == false) {
            this.MealsArray[this.tabNum][index].selected[this.mealNum].qnt = 1;
        }
        //this.checkSelected(index, 0)
    };
    ;
    MealsMenuPage.prototype.checkSelected = function (index, type) {
        if (type === void 0) { type = null; }
        this.currentSelected = this.checkMealLimits();
        //057782054
        // if (this.currentSelected > this.currentLimit) {
        //     setTimeout(() => {
        //         if (type == 0) {
        //             console.log("Selected")
        //             this.MealsArray[this.tabNum][index].selected[this.mealNum].qnt = 1;
        //             this.MealsArray[this.tabNum][index].selected[this.mealNum].selected = false;
        //         }
        //     }, 10);
        //     setTimeout(() => {
        //         //alert("עברת את המכסה שלך עבור ארוחה זו");
        //         let alert = this.alertCtrl.create({
        //             //title: 'הגבלת כמות',
        //             subTitle: 'עברת את המכסה שלך עבור ארוחה זו',
        //             buttons: ['סגור']
        //         });
        //         alert.present();
        //
        //     }, 100);
        //     return false;
        // }
        return true;
    };
    MealsMenuPage.prototype.selectedClicked = function () {
    };
    MealsMenuPage.prototype.qntAlert = function (i, id) {
        var _this = this;
        var checked = this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt - 1;
        var alert = this.alertCtrl.create();
        alert.setTitle('בחר כמות מנות');
        alert.setCssClass('alertQnt');
        alert.addInput({
            type: 'radio',
            label: 'מנה 1',
            value: '1',
            handler: function (data) {
                _this.updateQnt(1, id);
                console.log("1 : ", _this.MealsArray, _this.tabNum, i, _this.mealNum);
                //this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 1;
            }
        });
        alert.addInput({
            type: 'radio',
            label: '2 מנות',
            value: '2',
            handler: function (data) {
                _this.updateQnt(2, id);
                console.log("2 : ", _this.MealsArray, _this.tabNum, i, _this.mealNum);
                //this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 2;
            }
        });
        alert.addInput({
            type: 'radio',
            label: '3 מנות',
            value: '3',
            handler: function (data) {
                _this.updateQnt(3, id);
                console.log("3 : ", _this.MealsArray, _this.tabNum, i, _this.mealNum);
                //this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 3;
            }
        });
        alert.addInput({
            type: 'radio',
            label: '4 מנות',
            value: '4',
            handler: function (data) {
                _this.updateQnt(4, id);
                //console.log("4 : " , this.MealsArray ,this.tabNum , i ,  this.mealNum)
                //this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 4;
            }
        });
        alert.data.inputs[checked].checked = true;
        //this.MealsArray[this.tabNum][i].selected[this.mealNum].selected
        //alert.addButton('מחק');
        alert.addButton({
            text: 'מחק',
            handler: function (data) {
                _this.MealsArray[_this.tabNum][i].selected[_this.mealNum].selected = false;
                _this.MealsArray[_this.tabNum][i].selected[_this.mealNum].qnt = 1;
            }
        });
        alert.addButton({
            text: 'אשר',
            handler: function (data) {
                _this.MealsArray[_this.tabNum][i].selected[_this.mealNum].qnt = data;
                _this.checkLimit = _this.checkSelected(i);
                // //if (this.checkLimit == false)
                //     this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 1;
                return _this.checkLimit;
            }
        });
        alert.present();
    };
    MealsMenuPage.prototype.updateQnt = function (num, id) {
        //this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 3;
        var meal = this.MealsArray[this.tabNum].find(function (element) {
            return element.id == id;
        });
        meal.selected[this.mealNum].qnt = num;
        console.log("Meal :  ", this.MealsArray, id, meal, this.mealNum, num);
    };
    MealsMenuPage.prototype.checkMealLimits = function () {
        var Selected = 0;
        var mealNum = this.mealNum;
        this.MealsArray[this.tabNum].forEach(function (product, index) {
            if (product.selected[mealNum].selected == true) {
                Selected += Number(product.selected[mealNum].qnt);
            }
        });
        return Selected;
    };
    // checkLimits() {
    //     let Selected = 0;
    //     let mealNum = this.mealNum;
    //
    //     for (let i = 0; i < this.MealsArray.length; i++) {
    //         Selected = 0;
    //
    //         for (let j = 0; j < this.MealsArray[i].length; j++) {
    //             if (this.MealsArray[i][j].selected[mealNum].selected == true)
    //                 Selected += Number(this.MealsArray[i][j].selected[mealNum].qnt);
    //         }
    //
    //         if (Selected != this.MealsService.MenuLimits[this.mealNum][i].qnt)
    //             return false;
    //     }
    //
    //     return true;
    // }
    MealsMenuPage.prototype.checkLimits = function () {
        var Selected = 0;
        var mealNum = this.mealNum;
        var mealsWarning = [];
        console.log("MA : ", this.MealsArray);
        for (var i = 0; i < this.MealsArray.length; i++) {
            Selected = 0;
            for (var j = 0; j < this.MealsArray[i].length; j++) {
                if (this.MealsArray[i][j].selected[mealNum].selected == true)
                    Selected += Number(this.MealsArray[i][j].selected[mealNum].qnt);
            }
            var obj = void 0;
            //obj.type = this.MealsArray[i];
            if (Selected == this.MealsService.MenuLimits[this.mealNum][i].qnt)
                obj = 0;
            else if (Selected < this.MealsService.MenuLimits[this.mealNum][i].qnt)
                obj = -1;
            else if (Selected > this.MealsService.MenuLimits[this.mealNum][i].qnt)
                obj = 1;
            mealsWarning.push(obj);
        }
        return mealsWarning;
    };
    MealsMenuPage.prototype.SaveMeal = function (MealId) {
        var _this = this;
        var Limits = this.checkLimits();
        var message = '';
        // if(Limits[0] == 1)
        //     message += ' בארוחה זו אכלת פחמימות מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[0] == -1)
        //     message += ' בארוחה זו אכלת פחמימות מתחת הכמות הקבועה לך בתפריט '
        // if(Limits[1] == 1)
        //     message += ' בארוחה זו אכלת חלבונים מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[1] == -1)
        //     message += ' בארוחה זו אכלת חלבונים מתחת הכמות הקבועה לך בתפריט '
        // if(Limits[2] == 1)
        //     message += ' בארוחה זו אכלת שומן מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[2] == -1)
        //     message += ' בארוחה זו אכלת שומן מתחת הכמות הקבועה לך בתפריט ';
        // if(Limits[3] == 1)
        //     message += ' בארוחה זו אכלת פרי מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[3] == -1)
        //     message += ' בארוחה זו אכלת פרי מתחת הכמות הקבועה לך בתפריט ';
        //
        // let alert = this.alertCtrl.create({
        //     title: 'הגבלת כמות',
        //     subTitle: message,
        //     buttons: ['סגור'],
        //     cssClass: 'alertQnt'
        // });
        // alert.present();
        console.log("SaveMeal : ", this.MealsArray);
        this.MealsService.SaveMeal('SaveMenu', this.MealsArray, MealId).then(function (data) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__meals_meals__["a" /* MealsPage */], { 'mealAccept': _this.mealNum });
        });
        // }
        // if (Limits) {
        //
        // }
        // else {
        //     setTimeout(() => {
        //         //alert("עברת את המכסה שלך עבור ארוחה זו");
        //         let alert = this.alertCtrl.create({
        //             title: 'הגבלת כמות',
        //             subTitle: 'לא השלמת את המכסה לארוחה זו , אנא נסה שוב',
        //             buttons: ['סגור'],
        //             cssClass: 'alertQnt'
        //         });
        //         alert.present();
        //
        //     }, 100);
        // }
        // setTimeout(() => {
        //     let str = ''
        //
        //     //alert("עברת את המכסה שלך עבור ארוחה זו");
        //     let alert = this.alertCtrl.create({
        //         title: 'הגבלת כמות',
        //         subTitle: 'לא השלמת את המכסה לארוחה זו , אנא נסה שוב',
        //         buttons: ['סגור'],
        //         cssClass: 'alertQnt'
        //     });
        //     alert.present();
        //
        //     this.MealsService.SaveMeal('SaveMenu', this.MealsArray, MealId).then((data: any) => {
        //         this.navCtrl.push(MealsPage, {'mealAccept': this.mealNum});
        //     });
        // }, 100);
    };
    MealsMenuPage.prototype.CancelMeal = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__meals_meals__["a" /* MealsPage */]);
    };
    MealsMenuPage.prototype.setFilteredItems = function () {
        this.items = this.filterItems(this.mealName);
    };
    MealsMenuPage.prototype.filterItems = function (searchTerm) {
        var mealCopy = this.MealsArray.slice();
        if (!searchTerm)
            searchTerm = '';
        console.log(searchTerm, this.MealsArray, this.tabNum);
        if (mealCopy) {
            return mealCopy[this.tabNum].filter(function (item) {
                // console.log("TTT : ", item)
                return item['title'].toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
    };
    MealsMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-meals-menu',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\meals-menu\meals-menu.html"*/'<!--\n\n  Generated template for the MealsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n    <ion-toolbar no-border-top class="ToolBarClass">\n\n        <div class="ToolBarBackButton">\n\n            <button ion-button icon-only clear navPop>\n\n                <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n            </button>\n\n        </div>\n\n        <div class="ToolBarTitle">\n\n            <ion-title>\n\n                <img src="images/head_logo.png" class="headLogo" />\n\n            </ion-title>\n\n        </div>\n\n    </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="Content">\n\n\n\n    <!-- Title Text -->\n\n    <!--<div class="MealsInfo">-->\n\n    <!--<div class="MealsTitle">-->\n\n    <!--בחירת ארוחות-->\n\n    <!--</div>-->\n\n    <!--</div>-->\n\n    <div class="filterRight" style="width: 98%; margin: 0 auto; ">\n\n        <ion-searchbar [(ngModel)]="mealName" (ionInput)="setFilteredItems($event)" placeholder="חפש מנה"\n\n            style="text-align: right !important; direction: rtl !important;"></ion-searchbar>\n\n    </div>\n\n\n\n\n\n\n\n\n\n\n\n    <div class="TabsButton" align="center" *ngIf="isDataAvailable1 && isDataAvailable2">\n\n        <div class="TabsButtonDiv">\n\n\n\n            <div class="scrolling-wrapper" *ngIf="MenuLimits.length > 0">\n\n                <div *ngFor="let item of MenuLimits[mealNum] let i = index" class="card">\n\n                    <div *ngIf="item.qnt > -1" class="TabsButtonLeft">\n\n                        <div [ngClass]="tabNum == i ? \'GroupSelected headBTN\' : \'PersonalSelected  headBTN\'"\n\n                            (click)="changeTab(item.id,i)">{{item.title}} - {{item.qnt}}\n\n                        </div>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n\n\n            <!-- <div *ngIf="MenuLimits[mealNum][0].qnt > -1 " class="TabsButtonLeft">\n\n                    <button ion-button\n\n                            [ngClass]="{\'PersonalSelected\' : (tabNum == 0), \'GroupSelected\':(tabNum != 0)}"\n\n                            (click)="changeTab(MealsTypesArray[0].id,0)" full>{{MealsTypesArray[0].title}} - {{MenuLimits[mealNum][0].qnt}}\n\n                    </button>\n\n                </div>\n\n                <div *ngIf="MenuLimits[mealNum][2].qnt > -1 " class="TabsButtonRight">\n\n                    <button ion-button\n\n                            [ngClass]="{\'PersonalSelected\' : (tabNum == 2), \'GroupSelected\':(tabNum != 2)}"\n\n                            (click)="changeTab(MealsTypesArray[2].id,2)" full>{{MealsTypesArray[2].title}} - {{MenuLimits[mealNum][2].qnt}}\n\n                    </button>\n\n                </div>\n\n                <div *ngIf="MenuLimits[mealNum][1].qnt > -1" class="TabsButtonCenter">\n\n                    <button ion-button\n\n                            [ngClass]="{\'PersonalSelected\' : (tabNum == 1), \'GroupSelected\':(tabNum != 1)}"\n\n                            (click)="changeTab(MealsTypesArray[1].id,1)" full>{{MealsTypesArray[1].title}} - {{MenuLimits[mealNum][1].qnt}}\n\n                    </button>\n\n                </div>\n\n                <div *ngIf="MenuLimits[mealNum][3].qnt > -1" class="TabsButtonRight">\n\n                    <button ion-button\n\n                            [ngClass]="{\'PersonalSelected\' : (tabNum == 3), \'GroupSelected\':(tabNum != 3)}"\n\n                            (click)="changeTab(MealsTypesArray[3].id,3)" full>{{MealsTypesArray[3].title}} - {{MenuLimits[mealNum][3].qnt}}\n\n                    </button>\n\n                </div> -->\n\n        </div>\n\n    </div>\n\n\n\n\n\n    <!-- Meals Checkbox -->\n\n    <div class="Question">\n\n\n\n\n\n        <ion-list class="CheckBoxDiv" radio-group [(ngModel)]="relationship">\n\n            <div *ngFor="let item of items let i=index">\n\n                <!-- [ngClass]="item.selected[mealNum].selected ? \'zindexSelected\' : \'zindexNonSelected\'" -->\n\n                <ion-item class="CheckBoxDivElement">\n\n                    <ion-checkbox (ionChange)="itemClicked(mealNum,item.id,i)"\n\n                        [(ngModel)]="item.selected[mealNum].selected"></ion-checkbox>\n\n                    <ion-label class="titleLable">\n\n                        {{item.title}}\n\n                        <p class="mealTitle"> גודל מנה : {{item.description}} </p>\n\n                    </ion-label>\n\n\n\n                    <ion-label *ngIf="item.selected[mealNum].selected" (click)="qntAlert(i,item.id)" item-end ion-button\n\n                        class="MealsSelected">{{item.selected[mealNum].qnt}} מנות\n\n                    </ion-label>\n\n\n\n                    <!-- <ion-select  *ngIf="item.selected[mealNum].selected" (ionChange)="selectedClicked()" class="MealsSelected" [(ngModel)]="item.selected[mealNum].qnt" cancelText="בטל" okText="בחר" style="z-index:0;" >\n\n                          <ion-option class="MealsSelected" value=1 >&nbsp;  מנה 1  &nbsp; &nbsp;</ion-option>\n\n                          <ion-option class="MealsSelected" value=2>2 מנות   &nbsp; &nbsp;</ion-option>\n\n                          <ion-option class="MealsSelected" value=3 >3 מנות  &nbsp; &nbsp;</ion-option>\n\n                      </ion-select> -->\n\n                </ion-item>\n\n            </div>\n\n        </ion-list>\n\n        <!-- <button ion-button class="IntroButton" (click)="questionClicked()">לשאלה הבאה</button> -->\n\n    </div>\n\n</ion-content>\n\n\n\n<ion-footer class="footerClass">\n\n    <table>\n\n        <tr>\n\n            <td (click)="CancelMeal()">\n\n                <img src="images/s2.png" class="imageclass" />\n\n            </td>\n\n            <td (click)="SaveMeal(mealNum)">\n\n                <img src="images/s1.png" class="imageclass" />\n\n            </td>\n\n        </tr>\n\n    </table>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\meals-menu\meals-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_MealsService__["a" /* MealsService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */]])
    ], MealsMenuPage);
    return MealsMenuPage;
}());

//# sourceMappingURL=meals-menu.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PopupPage = /** @class */ (function () {
    function PopupPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.img_url = this.navParams.get('url');
        console.log(this.img_url);
    }
    PopupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PopupPage');
    };
    PopupPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    PopupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-popup',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\popup\popup.html"*/'<!--\n\n  Generated template for the PopupPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-content text-center>\n\n\n\n    <!-- <ion-row>\n\n        <ion-col text-right padding-right>\n\n            <ion-icon name="close" style="font-size: 30px;" (click)="closeModal()"></ion-icon>\n\n        </ion-col>\n\n    </ion-row> -->\n\n\n\n    <button ion-button class="IntroButtonClose" (click)="closeModal()">סגור</button>\n\n\n\n    <div class="div100" align="center" (click)="closeModal()">\n\n        <div class="div90">\n\n            <img src="{{img_url}}" class="w100" />\n\n        </div>\n\n    </div>\n\n    <button ion-button class="IntroButton">יש לשתות 2 כוסות מים</button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\popup\popup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* ViewController */]])
    ], PopupPage);
    return PopupPage;
}());

//# sourceMappingURL=popup.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_QuestionService__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the QuestionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var QuestionsPage = /** @class */ (function () {
    function QuestionsPage(toastCtrl, navCtrl, navParams, Settings, QuestionService) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Settings = Settings;
        this.QuestionService = QuestionService;
        this.QuestionsArray = [];
        this.AnswerArray = [];
        this.isAnswered = false;
        this.QuestionCounter = -1;
        this.Score = 0;
        this.Contact = {
            'name': '',
            'mail': '',
            'details': '',
            'phone': '',
        };
    }
    QuestionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad QuestionsPage');
    };
    QuestionsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.QuestionService.getAllQuestions('getQuestions').then(function (data) {
            console.log("Questions: ", data), _this.QuestionsArray = data, _this.fillAnswerArray();
        });
        //this.QuestinsArray = this.QuestionService.getQuestionsArray();
        console.log("Questions ", this.QuestionsArray);
    };
    QuestionsPage.prototype.fillAnswerArray = function () {
        for (var i = 0; i < this.QuestionsArray.length; i++)
            this.AnswerArray.push('');
    };
    QuestionsPage.prototype.questionClicked = function () {
        this.QuestionCounter++;
        if (this.QuestionCounter == (this.QuestionsArray.length)) {
            this.Score = this.calculateScore();
            this.isAnswered = true;
        }
        console.log('this.AnswerArray : ', this.AnswerArray);
    };
    QuestionsPage.prototype.endQuiz = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
    };
    QuestionsPage.prototype.calculateScore = function () {
        var score = 0;
        for (var i = 0; i < this.AnswerArray.length; i++) {
            score += Number(this.AnswerArray[i]);
        }
        return score;
    };
    QuestionsPage.prototype.goBack = function () {
        //this.navCtrl.setRoot(HomePage);
        this.navCtrl.popToRoot();
    };
    QuestionsPage.prototype.SendForm = function () {
        var _this = this;
        if (this.Contact.name.length < 3)
            this.presentToast("יש למלא שם מלא", 0);
        else if (this.Contact.mail.length < 3)
            this.presentToast("יש למלא מייל", 0);
        else if (this.Contact.phone.length < 3)
            this.presentToast("יש למלא טלפון", 0);
        else if (this.Contact.details.length < 3)
            this.presentToast("יש למלא תוכן הפנייה", 0);
        else {
            //this.Settings.ContactDetails = this.Contact;
            //this.Server.sendContactDetails('getContact');
            this.QuestionService.sendDetails('sendDetails', this.Contact, this.Score).then(function (data) {
                console.log("Questions: ", data), _this.QuestionsArray = data, _this.fillAnswerArray();
            });
            this.presentToast("תודה נחזור אליך בהקדם", 1);
            this.Contact.name = '';
            this.Contact.mail = '';
            this.Contact.details = '';
            this.Contact.phone = '';
        }
    };
    QuestionsPage.prototype.presentToast = function (message, type) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            cssClass: 'Toast'
        });
        toast.onDidDismiss(function () {
            if (type == 1)
                _this.endQuiz();
        });
        toast.present();
    };
    QuestionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-questions',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\questions\questions.html"*/'\n\n<ion-header>\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button  ion-button icon-only clear navPop>\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo"  />\n\n      </ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <ion-slides autoplay="3000" loop="true" speed="500" class="slides" effect="coverflow"  pager="true">\n\n        <ion-slide>\n\n            <img src="images/home/s6.jpg" style="width:100%;" />\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="images/home/s2.jpg" style="width:100%" />\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="images/home/s3.jpg" style="width:100%" />\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="images/home/s4.jpg" style="width:100%;" />\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="images/home/s5.jpg" style="width:100%" />\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="images/home/s1.jpg" style="width:100%" />\n\n        </ion-slide>\n\n    </ion-slides>\n\n\n\n  <div class="backgroundImage">\n\n    <img src="images/trainings_bck.jpg" class="imageclass"/>\n\n  </div>\n\n\n\n\n\n\n\n  <!-- QuestionInfo  -->\n\n  <div class="QuestionInfo">\n\n    <div class="QuestionTitle">\n\nבדיקת התאמה\n\n    </div>\n\n\n\n    <!-- Intro Quiz -->\n\n    <div class="IntroQuestion" *ngIf="QuestionCounter == -1 && !isAnswered">\n\n        <div style="font-weight: bold;"><br>  <span style="font-weight: bold; color: #1fbbf1"> 647</span> הינה תוכנית לכושר ותזונה המובילה<br>אנשים לשינוי בדרך החיים שלהם.\n\n            <br>            על מנת להשתתף בתוכנית <br>      עליך לענות על השאלות הבאות    <br></div>\n\n        <button ion-button class="IntroButton" (click)="questionClicked()">בדוק התאמה</button>\n\n    </div>\n\n\n\n    <!-- Question Quiz -->\n\n    <div class="Question" *ngIf="QuestionCounter > -1 && !isAnswered">\n\n      <div class="QuestionText">{{QuestionsArray[QuestionCounter].title}}</div>\n\n      <ion-list class="CheckBoxDiv" radio-group [(ngModel)]="AnswerArray[QuestionCounter]">\n\n        <ion-item class="CheckBoxDivElement">\n\n          <ion-label class="AnswerLabel">{{QuestionsArray[QuestionCounter].answer1}}</ion-label>\n\n          <ion-radio value=1 checked = "false"></ion-radio>\n\n        </ion-item>\n\n        <ion-item class="CheckBoxDivElement">\n\n          <ion-label class="AnswerLabel">{{QuestionsArray[QuestionCounter].answer2}}</ion-label>\n\n          <ion-radio value=2 checked = "false"></ion-radio>\n\n        </ion-item>\n\n        <ion-item class="CheckBoxDivElement">\n\n          <ion-label class="AnswerLabel">{{QuestionsArray[QuestionCounter].answer3}}</ion-label>\n\n          <ion-radio value=3 checked = "false"></ion-radio>\n\n        </ion-item>\n\n        <ion-item class="CheckBoxDivElement">\n\n          <ion-label class="AnswerLabel">{{QuestionsArray[QuestionCounter].answer4}}</ion-label>\n\n          <ion-radio value=4 checked = "false"></ion-radio>\n\n        </ion-item>\n\n      </ion-list>\n\n      <button ion-button class="IntroButton" (click)="questionClicked()">לשאלה הבאה</button>\n\n    </div>\n\n\n\n    <!-- End Quiz -->\n\n    <div class="IntroQuestion" *ngIf="isAnswered == true">\n\n      <div *ngIf="Score < 12"><br>נמצאת מתאים להשתתף בתכנית<br>אנא מלא את הפרטים בטופס שלפניך<br>ונציג <span style="color: #56c9ef; font-weight:bold" >647</span> יחזור אליך</div>\n\n        <div *ngIf="Score > 11"><br>נראה שאתה עדיין לא נחוש<br>אנא מלא את הטופס שלפניך  <br>ונציג מטעמנו יבחן את השתתפותך בתכנית</div>\n\n        <!--<button ion-button class="IntroButton" (click)="endQuiz()">שלח תוצאות</button>-->\n\n\n\n        <div class="loginMain patternBackground">\n\n            <div class="loginTitle">\n\n                צור קשר\n\n            </div>\n\n            <div class="InputsText" align="center">\n\n                <div class="InputText">\n\n                    <input type="text" placeholder=" שם מלא" [(ngModel)]="Contact.name" />\n\n                </div>\n\n                <div class="InputText mt10">\n\n                    <input type="text" placeholder=" מספר טלפון" [(ngModel)]="Contact.phone" />\n\n                </div>\n\n                <div class="InputText mt10">\n\n                    <input type="text" placeholder=" אימייל" [(ngModel)]="Contact.mail" />\n\n                </div>\n\n                <div class="InputText mt10">\n\n                    <textarea [(ngModel)]="Contact.details" name="info" placeholder=" קצת פרטים עלייך" rows="4"></textarea>\n\n                </div>\n\n\n\n                <button ion-button class="IntroButton" (click)="SendForm()">שלח פרטים</button>\n\n            </div>\n\n        </div>\n\n    </div>\n\n\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\questions\questions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_3__services_QuestionService__["a" /* QuestionService */]])
    ], QuestionsPage);
    return QuestionsPage;
}());

//# sourceMappingURL=questions.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_QuestionService__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CustomersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CustomersPage = /** @class */ (function () {
    function CustomersPage(navCtrl, sanitizer, navParams, questionService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.sanitizer = sanitizer;
        this.navParams = navParams;
        this.questionService = questionService;
        this.questionService.getAllQuestions('getQuestions').then(function (data) {
            console.log("Movies : ", data);
            _this.movies = data.filter(function (item) {
                console.log("It : ", item);
                return item['answer1'] == 3;
            });
            // this.movies = this.movies['title'];
            console.log("MoviesCustomers : ", _this.movies);
        });
    }
    CustomersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CustomersPage');
    };
    CustomersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-customers',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\customers\customers.html"*/'<ion-header>\n  <ion-toolbar no-border-top class="ToolBarClass">\n    <div class="ToolBarBackButton">\n      <button ion-button icon-only clear navPop>\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n      </button>\n    </div>\n    <div class="ToolBarTitle">\n      <ion-title>\n        <img src="images/head_logo.png" class="headLogo" />\n      </ion-title>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class="Content">\n  <!-- <div class="backgroundImage">\n    <img src="images/menu_bck.jpg" class="imageclass" />\n  </div> -->\n  <div padding>\n    <a href="https://www.youtube.com/channel/UCQtbZ_00gNELECd5scYquYA/featured">\n      <button ion-button class="youtubeBTN">לערוץ שלנו ב YOUTUBE</button></a>\n    <div class="mt-3">\n      <div *ngFor="let movie of movies" class="mt-2">\n        <!-- <iframe *ngIf="movie != \'\'" width="100%" height="230"\n          [src]="sanitizer.bypassSecurityTrustResourceUrl(\'https://www.youtube.com/embed/\'+movie.title | safe)"\n          frameborder="0" allowfullscreen></iframe> -->\n        <iframe *ngIf="movie != \'\'" class="youTubeVideo" [src]="\'https://www.youtube.com/embed/\'+movie.title | safe"\n          frameborder="0" allowfullscreen width="100%" height="230"></iframe>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\customers\customers.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_QuestionService__["a" /* QuestionService */]])
    ], CustomersPage);
    return CustomersPage;
}());

//# sourceMappingURL=customers.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DayAfterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trainings_trainings__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the DayAfterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DayAfterPage = /** @class */ (function () {
    function DayAfterPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DayAfterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DayAfterPage');
    };
    DayAfterPage.prototype.gotoWeightPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__trainings_trainings__["a" /* TrainingsPage */]);
    };
    DayAfterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-day-after',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\day-after\day-after.html"*/'<!--\n\n  Generated template for the DayAfterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button  ion-button icon-only clear navPop>\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo"  />\n\n      </ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="Content">\n\n  <div class="mainImage">\n\n    <img src="images/after/bck1.png" style="width: 100%" />\n\n  </div>\n\n\n\n  <div class="mainButton" (click)="gotoWeightPage()">\n\n    <img src="images/after/11.png" style="width: 100%;" />\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\day-after\day-after.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */]])
    ], DayAfterPage);
    return DayAfterPage;
}());

//# sourceMappingURL=day-after.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoviesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_QuestionService__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the MoviesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoviesPage = /** @class */ (function () {
    function MoviesPage(navCtrl, navParams, questionService, sanitizer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.questionService = questionService;
        this.sanitizer = sanitizer;
        this.selected = 5;
        this.questionService.getAllQuestions('getQuestions').then(function (data) {
            _this.allMovies = data;
            _this.changeStstus(_this.selected);
        });
    }
    MoviesPage.prototype.goToRegister = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */]);
    };
    MoviesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MoviesPage');
    };
    MoviesPage.prototype.changeStstus = function (status) {
        var _this = this;
        this.selected = status;
        this.movies = this.allMovies.filter(function (item) {
            return item['answer1'] == _this.selected;
        });
    };
    MoviesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-movies',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\movies\movies.html"*/'<ion-header>\n  <ion-toolbar no-border-top class="ToolBarClass">\n    <div class="ToolBarBackButton">\n      <button ion-button icon-only clear (click)="goToRegister()">\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n      </button>\n    </div>\n    <div class="ToolBarTitle">\n      <ion-title>\n        <img src="images/head_logo.png" class="headLogo" />\n      </ion-title>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <div class="backgroundImage">\n    <img src="images/trainings_bck.jpg" class="imageclass" />\n  </div>\n\n\n  <div class="row btns">\n    <div [ngClass]="selected == 4 ? \'col-6 regBtn btnSelected\' : \'col-6 regBtn\'" (click)="changeStstus(4)">\n      אימון פיזי\n    </div>\n    <div [ngClass]="selected == 5 ? \'col-6 regBtn btnSelected\' : \'col-6 regBtn\'" (click)="changeStstus(5)">\n      אימון מנטלי\n    </div>\n  </div>\n\n  <div *ngFor="let movie of movies" class="mt-2">\n    <!-- <iframe *ngIf="movie != \'\'" width="100%" height="230"\n      [src]="sanitizer.bypassSecurityTrustResourceUrl(\'https://www.youtube.com/embed/\'+movie.title)" frameborder="0"\n      allowfullscreen></iframe> -->\n\n      <iframe *ngIf="movie != \'\'" class="youTubeVideo" [src]="\'https://www.youtube.com/embed/\'+movie.title | safe"\n      frameborder="0" allowfullscreen width="100%" height="230"></iframe>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\movies\movies.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_QuestionService__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */]])
    ], MoviesPage);
    return MoviesPage;
}());

//# sourceMappingURL=movies.js.map

/***/ }),

/***/ 190:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 190;

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//const UserId = "https://tapper.org.il/647/laravel/public/api/";
var UserId = "https://team647.com/laravel/public/api/";
var Config = /** @class */ (function () {
    function Config() {
        this.UserId = '';
        this.PushId = '';
        this.FullName = '';
        this.version = '2';
        this.phpHost = 'https://team647.com/647-app/php/';
        this.ServerUrl = "https://team647.com/laravel/public/api/";
        this.CourseId = '';
    }
    ;
    Config.prototype.SetUserPush = function (push_id) {
        console.log("PID : ", push_id);
        this.PushId = push_id;
        window.localStorage.push_id = push_id;
    };
    Config = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], Config);
    return Config;
}());

;
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 234:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		727,
		17
	],
	"../pages/add-training/add-training.module": [
		728,
		16
	],
	"../pages/add/add.module": [
		729,
		0
	],
	"../pages/addweight/addweight.module": [
		733,
		15
	],
	"../pages/chat-modal/chat-modal.module": [
		730,
		14
	],
	"../pages/chat/chat.module": [
		731,
		13
	],
	"../pages/customers/customers.module": [
		732,
		12
	],
	"../pages/day-after/day-after.module": [
		734,
		11
	],
	"../pages/login/login.module": [
		735,
		10
	],
	"../pages/meals-menu/meals-menu.module": [
		736,
		9
	],
	"../pages/meals/meals.module": [
		737,
		8
	],
	"../pages/movies/movies.module": [
		738,
		7
	],
	"../pages/popup/popup.module": [
		739,
		6
	],
	"../pages/questions/questions.module": [
		741,
		5
	],
	"../pages/register/register.module": [
		740,
		4
	],
	"../pages/singl-group-weight/singl-group-weight.module": [
		742,
		3
	],
	"../pages/trainings/trainings.module": [
		743,
		2
	],
	"../pages/weight/weight.module": [
		744,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 234;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\list\list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>List</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n\n      <ion-icon [name]="item.icon" item-left></ion-icon>\n\n      {{item.title}}\n\n      <div class="item-note" item-right>\n\n        {{item.note}}\n\n      </div>\n\n    </button>\n\n  </ion-list>\n\n  <div *ngIf="selectedItem" padding>\n\n    You navigated here from <b>{{selectedItem.title}}</b>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\list\list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__safe_safe__ = __webpack_require__(696);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__safe_safe__["a" /* SafePipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__safe_safe__["a" /* SafePipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_config__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ChatModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ChatModalPage = /** @class */ (function () {
    function ChatModalPage(navCtrl, navParams, viewCtrl, Settings, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.Settings = Settings;
        this.platform = platform;
        this.imagePath = this.navParams.get('imagePath');
        this.isAndroid = false;
        this.phpHost = Settings.phpHost;
        if (this.platform.is('android')) {
            this.isAndroid = true;
        }
        platform.registerBackButtonAction(function () {
            _this.viewCtrl.dismiss();
        }, 1);
    }
    ChatModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatModalPage');
    };
    ChatModalPage.prototype.ngOnInit = function () {
    };
    ChatModalPage.prototype.closeImageModal = function () {
        this.viewCtrl.dismiss();
    };
    ChatModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chat-modal',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\chat-modal\chat-modal.html"*/'<!--\n\n  Generated template for the ChatModalPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n\n\n    <div class="ToolBarBackButton">\n\n      <button  ion-button icon-only clear navPop>\n\n        <ion-icon name="ios-close-circle" (click)="closeImageModal()"></ion-icon>\n\n      </button>\n\n    </div>\n\n\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n\n\n  <div style="width:100%; margin-top:55px;" align="center">\n\n\n\n    <!--\n\n      <zoom-area>\n\n        <img  src="{{phpHost+imagePath}}" style="width:100%;"/>\n\n      </zoom-area>\n\n  -->\n\n\n\n    <img  src="{{phpHost+imagePath}}" style="width:100%;" imageViewer  />\n\n\n\n\n\n  </div>\n\n\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\chat-modal\chat-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Platform */]])
    ], ChatModalPage);
    return ChatModalPage;
}());

//# sourceMappingURL=chat-modal.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SinglGroupWeightPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SinglGroupWeightPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SinglGroupWeightPage = /** @class */ (function () {
    function SinglGroupWeightPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SinglGroupWeightPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SinglGroupWeightPage');
    };
    SinglGroupWeightPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-singl-group-weight',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\singl-group-weight\singl-group-weight.html"*/'<!--\n\n  Generated template for the SinglGroupWeightPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button  ion-button icon-only clear navPop>\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo"  />\n\n      </ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n\n\n\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\singl-group-weight\singl-group-weight.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */]])
    ], SinglGroupWeightPage);
    return SinglGroupWeightPage;
}());

//# sourceMappingURL=singl-group-weight.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(390);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(713);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_register_register__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_add_training_add_training__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_TraingngsService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__models_TrainingModel__ = __webpack_require__(714);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_weight_weight__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_singl_group_weight_singl_group_weight__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_weightService__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_questions_questions__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_QuestionService__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_meals_menu_meals_menu__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_meals_meals__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_MealsService__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_trainings_trainings__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_about_about__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_login_login__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_loginService__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_push__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_badge__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_popup_popup__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_firebase__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_day_after_day_after__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__directives_autosize_autosize__ = __webpack_require__(715);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_file_transfer__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_file__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_camera__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_file_path__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_chat_modal_chat_modal__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39_ionic2_zoom_area__ = __webpack_require__(716);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__angular_platform_browser_animations__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41_ionic_img_viewer__ = __webpack_require__(720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_customers_customers__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_movies_movies__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_addweight_addweight__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__ionic_native_youtube_video_player__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pipes_pipes_module__ = __webpack_require__(382);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_trainings_trainings__["a" /* TrainingsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_add_training_add_training__["a" /* AddTrainingPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_weight_weight__["a" /* WeightPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_singl_group_weight_singl_group_weight__["a" /* SinglGroupWeightPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_questions_questions__["a" /* QuestionsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_meals_meals__["a" /* MealsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_meals_menu_meals_menu__["a" /* MealsMenuPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_popup_popup__["a" /* PopupPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_day_after_day_after__["a" /* DayAfterPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_33__directives_autosize_autosize__["a" /* AutosizeDirective */],
                __WEBPACK_IMPORTED_MODULE_38__pages_chat_modal_chat_modal__["a" /* ChatModalPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_customers_customers__["a" /* CustomersPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_movies_movies__["a" /* MoviesPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_addweight_addweight__["a" /* AddweightPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-training/add-training.module#AddTrainingPageModule', name: 'AddTrainingPage', segment: 'add-training', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add/add.module#AddPageModule', name: 'AddPage', segment: 'add', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat-modal/chat-modal.module#ChatModalPageModule', name: 'ChatModalPage', segment: 'chat-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addweight/addweight.module#AddweightPageModule', name: 'AddweightPage', segment: 'addweight', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/day-after/day-after.module#DayAfterPageModule', name: 'DayAfterPage', segment: 'day-after', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meals-menu/meals-menu.module#MealsMenuPageModule', name: 'MealsMenuPage', segment: 'meals-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meals/meals.module#MealsPageModule', name: 'MealsPage', segment: 'meals', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/movies/movies.module#MoviesPageModule', name: 'MoviesPage', segment: 'movies', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popup/popup.module#PopupPageModule', name: 'PopupPage', segment: 'popup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/questions/questions.module#QuestionsPageModule', name: 'QuestionsPage', segment: 'questions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/singl-group-weight/singl-group-weight.module#SinglGroupWeightPageModule', name: 'SinglGroupWeightPage', segment: 'singl-group-weight', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trainings/trainings.module#TrainingsPageModule', name: 'TrainingsPage', segment: 'trainings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/weight/weight.module#WeightPageModule', name: 'WeightPage', segment: 'weight', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_40__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_39_ionic2_zoom_area__["a" /* ZoomAreaModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_41_ionic_img_viewer__["a" /* IonicImageViewerModule */],
                __WEBPACK_IMPORTED_MODULE_46__pipes_pipes_module__["a" /* PipesModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_trainings_trainings__["a" /* TrainingsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_add_training_add_training__["a" /* AddTrainingPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_weight_weight__["a" /* WeightPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_singl_group_weight_singl_group_weight__["a" /* SinglGroupWeightPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_questions_questions__["a" /* QuestionsPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_day_after_day_after__["a" /* DayAfterPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_meals_meals__["a" /* MealsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_meals_menu_meals_menu__["a" /* MealsMenuPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_popup_popup__["a" /* PopupPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_chat_modal_chat_modal__["a" /* ChatModalPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_customers_customers__["a" /* CustomersPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_movies_movies__["a" /* MoviesPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_addweight_addweight__["a" /* AddweightPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_firebase__["a" /* Firebase */],
                __WEBPACK_IMPORTED_MODULE_33__directives_autosize_autosize__["a" /* AutosizeDirective */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_45__ionic_native_youtube_video_player__["a" /* YoutubeVideoPlayer */],
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_file_path__["a" /* FilePath */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */],
                    useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* IonicErrorHandler */]
                }, __WEBPACK_IMPORTED_MODULE_11__services_TraingngsService__["a" /* TrainingsSevice */], __WEBPACK_IMPORTED_MODULE_12__models_TrainingModel__["a" /* TrainingModel */], __WEBPACK_IMPORTED_MODULE_13__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_16__services_weightService__["a" /* WeightService */], __WEBPACK_IMPORTED_MODULE_18__services_QuestionService__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_21__services_MealsService__["a" /* MealsService */], __WEBPACK_IMPORTED_MODULE_25__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_26__services_chatService__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_28__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_29__ionic_native_badge__["a" /* Badge */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trainings_trainings__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__weight_weight__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__meals_meals__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_loginService__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_TraingngsService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_weightService__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_MealsService__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__chat_chat__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__login_login__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__day_after_day_after__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__movies_movies__ = __webpack_require__(179);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
















/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, Login, Settings, ChatService, WeightService, TrainingsService, MealsService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.ChatService = ChatService;
        this.WeightService = WeightService;
        this.TrainingsService = TrainingsService;
        this.MealsService = MealsService;
        this.dayAfter = window.localStorage.dayAfter;
        this.getAllWeight();
        if (!window.localStorage.identify)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__login_login__["a" /* LoginPage */]);
    }
    RegisterPage.prototype.goHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    RegisterPage.prototype.gotoMovies = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_15__movies_movies__["a" /* MoviesPage */]);
    };
    RegisterPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.ionViewWillEnter = function () {
        this.getChatMessagesCount();
    };
    RegisterPage.prototype.gotoTrainings = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__trainings_trainings__["a" /* TrainingsPage */]);
    };
    RegisterPage.prototype.gotoWeightPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__weight_weight__["a" /* WeightPage */]);
    };
    RegisterPage.prototype.gotoMeals = function () {
        if (this.dayAfter == 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__meals_meals__["a" /* MealsPage */]);
    };
    RegisterPage.prototype.gotoChat = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__chat_chat__["a" /* ChatPage */]);
    };
    RegisterPage.prototype.gotoDayAfter = function () {
        if (this.dayAfter == 1)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__day_after_day_after__["a" /* DayAfterPage */]);
    };
    RegisterPage.prototype.LogOut = function () {
        this.Login.GetToken('GetToken', '');
        this.Login.logOutUser();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    RegisterPage.prototype.getAllWeight = function () {
        var _this = this;
        this.WeightService.getAllWeightByUserid('getWeight').then(function (data) {
            _this.getAllTrainings();
        });
    };
    RegisterPage.prototype.getAllTrainings = function () {
        var _this = this;
        this.TrainingsService.getTrainigTypes().then(function (data) {
        });
        this.TrainingsService.getAllTrainings('getTraining').then(function (data) {
            _this.getAllMenu();
        });
    };
    RegisterPage.prototype.getAllMenu = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.MealsService.getMeals('getMenu').then(function (data) {
                        })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.MealsService.getMealsTypes('getMenuTypes').then(function (data) {
                            })];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.MealsService.getMenuLimits('getMenuLimits').then(function (data) {
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.getChatMessagesCount = function () {
        var _this = this;
        this.ChatService.getChatMessagesCount('getChatMessagesCount').then(function (data) {
            _this.ChatMessageCount = data;
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\register\register.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n    <ion-toolbar no-border-top class="ToolBarClass">\n\n        <div class="ToolBarBackButton">\n\n            <button ion-button icon-only clear (click)="goHome()">\n\n                <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n            </button>\n\n        </div>\n\n        <div class="ToolBarTitle">\n\n            <ion-title>\n\n                <img src="images/head_logo.png" class="headLogo" />\n\n            </ion-title>\n\n        </div>\n\n        <button class="logOutButton" clear (click)="LogOut()">\n\n            <p>התנתק</p>\n\n        </button>\n\n    </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <div class="blueStrip">\n\n        <div class="blueStripOpacity"></div>\n\n        <div class="mainIcon">\n\n\n\n            <button ion-button clear class="Height100 no-padding"><img src="images/main_icon_1.png"\n\n                    (click)="gotoMeals()" class="imageButtonClass" />\n\n            </button>\n\n            <button ion-button clear class="Height100 no-padding"><img src="images/main_icon_2.png"\n\n                    (click)="gotoWeightPage()" class="imageButtonClass" /></button>\n\n            <button ion-button clear class="Height100 no-padding"><img src="images/main_icon_3.png"\n\n                    (click)="gotoTrainings()" class="imageButtonClass" /></button>\n\n            <button ion-button clear class="Height100 no-padding"><img src="images/main_icon_4.png" (click)="gotoChat()"\n\n                    class="imageButtonClass" /></button>\n\n            <button ion-button clear class="Height100 no-padding"><img src="images/main_icon_8.png"\n\n                    (click)="gotoMovies()" class="imageButtonClass" /></button>\n\n        </div>\n\n    </div>\n\n    <div class="backgroundImage">\n\n        <img src="images/appback.jpg" class="imageclass" />\n\n    </div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_7__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_13__services_chatService__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_9__services_weightService__["a" /* WeightService */], __WEBPACK_IMPORTED_MODULE_8__services_TraingngsService__["a" /* TrainingsSevice */], __WEBPACK_IMPORTED_MODULE_10__services_MealsService__["a" /* MealsService */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var QuestionService = /** @class */ (function () {
    function QuestionService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = this.Settings.ServerUrl;
        this.getAllQuestions('getQuestions');
    }
    ;
    QuestionService.prototype.getAllQuestions = function (url) {
        var _this = this;
        var body = new FormData();
        //body.append('uid', this.Settings.UserId.toString());
        return this.http.get(this.ServerUrl + '' + url).map(function (res) { return res.json(); }).do(function (data) {
            _this.QuestionsArray = data;
            console.log("QuestionsArray : ", _this.QuestionsArray);
        }).toPromise();
    };
    QuestionService.prototype.getQuestionsArray = function () {
        return this.QuestionsArray;
    };
    QuestionService.prototype.sendDetails = function (url, info, score) {
        var body = '&selected=' + JSON.stringify(info) + '&score=' + score;
        console.log("SAVVVE ", info);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) { console.log("Question : ", data); }).toPromise();
    };
    QuestionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], QuestionService);
    return QuestionService;
}());

;
//# sourceMappingURL=QuestionService.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingsSevice; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TrainingsSevice = /** @class */ (function () {
    function TrainingsSevice(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = this.Settings.ServerUrl;
    }
    ;
    TrainingsSevice.prototype.getAllTrainings = function (url) {
        var _this = this;
        //console.log("TS : " , this.Settings.UserId.toString())
        var body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.TrainingsArray = data;
        }).toPromise();
    };
    TrainingsSevice.prototype.addTraining = function (url, time, date, type) {
        var _this = this;
        var body = new FormData();
        body.append('date', date);
        body.append('time', time);
        body.append('type', type);
        body.append('uid', this.Settings.UserId.toString());
        //console.log("Body " , type +' :: '+ this.Settings.UserId + " : " ,time);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.TrainingsArray = data;
        }).toPromise();
    };
    TrainingsSevice.prototype.getTrainigTypes = function () {
        var _this = this;
        var body = new FormData();
        return this.http.post(this.ServerUrl + 'getTrainigTypes', body).map(function (res) { return res.json(); }).do(function (data) {
            _this.TrainingsTypes = data;
        }).toPromise();
    };
    TrainingsSevice.prototype.getTrainigTypesArray = function () {
        return this.TrainingsTypes;
    };
    TrainingsSevice.prototype.getAllTrainingsArray = function () {
        return this.TrainingsArray;
    };
    TrainingsSevice.prototype.checkWeaklyTraining = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + 'checkWeaklyTraining', body).map(function (res) { return res.json(); }).do(function (data) {
        }).toPromise();
    };
    TrainingsSevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], TrainingsSevice);
    return TrainingsSevice;
}());

;
//# sourceMappingURL=TraingngsService.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeightService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WeightService = /** @class */ (function () {
    function WeightService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = this.Settings.ServerUrl;
    }
    ;
    WeightService.prototype.getAllWeightByUserid = function (url) {
        var _this = this;
        var body = new FormData();
        //body.append('uid', this.Settings.CourseId.toString());
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.WeightArray = data; }).toPromise();
    };
    WeightService.prototype.getAllWeightArray = function () {
        console.log("AAA : ", this.WeightArray);
        return this.WeightArray;
    };
    WeightService.prototype.checkWeight = function (url) {
        var body = new FormData();
        //body.append('uid', this.Settings.CourseId.toString());
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    WeightService.prototype.addWeight = function (url, date, weight) {
        var body = new FormData();
        body.append('date', date);
        body.append('weight', String(weight));
        body.append('uid', this.Settings.UserId.toString());
        body.append('cid', localStorage.course_id);
        //addWeight
        console.log("Body ", url, date, weight, this.Settings);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    WeightService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], WeightService);
    return WeightService;
}());

;
//# sourceMappingURL=weightService.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatSevice; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChatSevice = /** @class */ (function () {
    function ChatSevice(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ChatArray = [];
        this._ChatArray = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.ChatArray$ = this._ChatArray.asObservable();
        this.ServerUrl = this.Settings.ServerUrl;
    }
    ;
    ChatSevice.prototype.addTitle = function (url, title, datetime, name, image, chatType) {
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        body.append('title', title);
        body.append('type', chatType);
        body.append('course_id', window.localStorage.course_id);
        body.append('date', datetime);
        body.append('name', name);
        body.append('image', image);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return ''; }).do(function (data) { console.log("Chat : ", data); }).toPromise();
    };
    ChatSevice.prototype.getChatDetails = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('user_id', window.localStorage.identify.toString());
        body.append('course_id', window.localStorage.course_id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.updateChatArray(data); }).toPromise();
    };
    ChatSevice.prototype.updateChatArray = function (data) {
        this.ChatArray = data;
        this._ChatArray.next({ text: this.ChatArray });
    };
    ChatSevice.prototype.pushToArray = function (data) {
        //this.ChatArray.push(data);
        this.ChatArray.splice((this.ChatArray.length), 0, data);
        this._ChatArray.next(this.ChatArray);
    };
    ChatSevice.prototype.registerPush = function (url, pushid) {
        var body = new FormData();
        body.append('push_id', pushid.toString());
        body.append('user_id', window.localStorage.identify.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return ''; }).do(function (data) { console.log("chat:", data); });
    };
    ChatSevice.prototype.getChatMessagesCount = function (url) {
        var body = new FormData();
        body.append('user_id', window.localStorage.identify.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { return data; }).toPromise();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* Content */])
    ], ChatSevice.prototype, "content", void 0);
    ChatSevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], ChatSevice);
    return ChatSevice;
}());

;
//# sourceMappingURL=chatService.js.map

/***/ }),

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SafePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SafePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    SafePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'safe',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], SafePipe);
    return SafePipe;
}());

//# sourceMappingURL=safe.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MealsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MealsService = /** @class */ (function () {
    function MealsService(http, Settings, loadingCtrl) {
        this.http = http;
        this.Settings = Settings;
        this.loadingCtrl = loadingCtrl;
        this.date = new Date();
        this.MenuArray = [];
        //public MenuLimits;
        this._MealsArray = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.MealsArray$ = this._MealsArray.asObservable();
        this._MenuLimits = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.MenuLimits$ = this._MenuLimits.asObservable();
        this._MealsTypesArray = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.MealsTypesArray$ = this._MealsTypesArray.asObservable();
        this.ServerUrl = this.Settings.ServerUrl;
    }
    Object.defineProperty(MealsService.prototype, "MealsArray", {
        get: function () { return this._MealsArray.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MealsService.prototype, "MenuLimits", {
        get: function () { return this._MenuLimits.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MealsService.prototype, "MealsTypesArray", {
        get: function () { return this._MealsTypesArray.getValue(); },
        enumerable: true,
        configurable: true
    });
    ;
    MealsService.prototype.getMeals = function (url) {
        // this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
        // let body = new FormData();
        // body.append('uid', this.Settings.UserId.toString());
        // body.append('date', this.Today);
        // //this._MealsArray.next(data);
        // return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{ this._MealsArray.next(data); /*console.log("getMeals : " , data)*/}).toPromise();
        var _this = this;
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        //loading.present();
        try {
            this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
            var body = new FormData();
            body.append('uid', this.Settings.UserId.toString());
            body.append('date', this.Today);
            return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this._MealsArray.next(data); }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            //loading.dismiss();
        }
    };
    MealsService.prototype.getMealsTypes = function (url) {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        //loading.present();
        try {
            var body = 'uid=' + this.Settings.UserId.toString();
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
                'Content-Type': 'multipart/form-data'
            });
            var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
                headers: headers
            });
            return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { _this._MealsTypesArray.next(data); }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            /// loading.dismiss();
        }
        // let body = 'uid=' + this.Settings.UserId.toString();
        //
        // let headers = new Headers({
        //   'Content-Type': 'multipart/form-data'
        // });
        //
        // let options = new RequestOptions({
        //   headers: headers
        // });
        //
        //   return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data)=>{this.MealsTypesArray = data/*, console.log("getMealsTypes : " , data) */}).toPromise();
    };
    MealsService.prototype.getMealsArray = function () {
        return this.MealsArray;
    };
    MealsService.prototype.getMealsTypesArray = function () {
        return this.MealsTypesArray;
    };
    MealsService.prototype.getMainPageMeals = function (url) {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        //loading.present();
        try {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
            var body = 'uid=' + this.Settings.UserId.toString() + '&date=' + this.Today;
            var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
                headers: headers
            });
            return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { _this.MenuArray = data; }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            //loading.dismiss();
        }
        // this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
        // let body = 'uid=' + this.Settings.UserId.toString() + '&date=' + this.Today;
        //
        // let headers = new Headers({
        //   'Content-Type': 'application/x-www-form-urlencoded'
        // });
        //
        // let options = new RequestOptions({
        //   headers: headers
        // });
        //
        // return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data:any)=>{ this.MenuArray = data;  }).toPromise();
    };
    MealsService.prototype.getMenuLimits = function (url) {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        // loading.present();
        try {
            var body = 'uid=' + this.Settings.UserId.toString();
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
                headers: headers
            });
            return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { _this._MenuLimits.next(data); }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            // loading.dismiss();
        }
        // let body = 'uid=' + this.Settings.UserId.toString();
        //
        // let headers = new Headers({
        //     'Content-Type': 'application/x-www-form-urlencoded'
        // });
        //
        // let options = new RequestOptions({
        //     headers: headers
        // });
        //
        // return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data:any)=>{/*console.log("MenuLimits : " , data, console.log("getMenuLimits : " , data)); */this.MenuLimits = data;}).toPromise();
    };
    MealsService.prototype.ItemClick = function (MealsNumber, ItemId) {
        this.Item = new Object();
        this.Item.MealsNumber = MealsNumber;
        this.Item.ItemId = ItemId;
        this.SelectedArray.push(this.Item);
    };
    MealsService.prototype.SaveMeal = function (url, Meals, MealId) {
        var _this = this;
        this.SelectedArray = Meals;
        this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
        var body = 'uid=' + this.Settings.UserId.toString() + '&selected=' + JSON.stringify(this.SelectedArray) + '&date=' + this.Today + '&mealId=' + MealId.toString();
        console.log("SAVVVE ", this.SelectedArray);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) { _this.isSave = data; console.log("SaveMeals : ", data); }).toPromise();
    };
    MealsService.prototype.acceptMeal = function (url, index) {
        var _this = this;
        this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
        var ChatDate = this.getDate();
        var body = 'uid=' + this.Settings.UserId.toString() + '&date=' + this.Today + '&chatDate=' + ChatDate + '&mealNum=' + index.toString();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) { _this.isSave = data; }).toPromise();
    };
    MealsService.prototype.getDate = function () {
        var date = new Date();
        var hours = this.date.getHours();
        var minutes = this.date.getMinutes();
        var seconds = this.date.getSeconds();
        if (hours < 10)
            hours = "0" + hours;
        if (minutes < 10)
            minutes = "0" + minutes;
        var time = hours + ':' + minutes;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '/' + mm + '/' + yyyy;
        var newdate = today + ' ' + time;
        return newdate;
    };
    MealsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["q" /* LoadingController */]])
    ], MealsService);
    return MealsService;
}());

;
//# sourceMappingURL=MealsService.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_training_add_training__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_TraingngsService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_register__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TrainingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TrainingsPage = /** @class */ (function () {
    function TrainingsPage(navCtrl, modalCtrl, navParams, TrainingsService, Settings) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.TrainingsService = TrainingsService;
        this.Settings = Settings;
        this.isDataAvailable = false;
        console.log("Tr1");
    }
    TrainingsPage.prototype.ionViewDidLoad = function () {
    };
    TrainingsPage.prototype.ngOnInit = function () {
        var _this = this;
        //this.TrainingsService.getTrainigTypes().then((data: any) => {this.TrainingsTypes  = data , console.log("Types : " , this.TrainingsTypes[0].title),this.isDataAvailable = true;})
        this.TrainingsService.getAllTrainings('getTraining').then(function (data) {
            _this.TrainingsArray = data.reverse(), console.log("All: ", _this.TrainingsArray);
        });
        this.TrainingsTypes = this.TrainingsService.getTrainigTypesArray();
        //this.TrainingsArray = this.TrainingsService.getAllTrainingsArray()
        console.log("Config : ", this.TrainingsTypes);
    };
    TrainingsPage.prototype.gotoAddTraining = function () {
        // this.navCtrl.push(AddTrainingPage)
        var AddTraining = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__add_training_add_training__["a" /* AddTrainingPage */], undefined, { cssClass: 'custom-modal' });
        AddTraining.onDidDismiss(function (data) {
            console.log(data);
        });
        AddTraining.present();
    };
    // getTrainingType(Id) {
    //     return this.TrainingsTypes[Id].title;
    // }
    TrainingsPage.prototype.goRegister = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__register_register__["a" /* RegisterPage */]);
    };
    TrainingsPage.prototype.changDateDirection = function (Date) {
        if (Date) {
            Date = Date.split('-');
            Date = Date[2] + '-' + Date[1] + '-' + Date[0];
            return Date;
        }
    };
    TrainingsPage.prototype.getTrainingType = function (type) {
        console.log("TP : ", type, this.TrainingsTypes);
        if (this.TrainingsTypes) {
            if (this.TrainingsTypes[type])
                return this.TrainingsTypes[type].title;
            else
                return "";
        }
        else
            return "";
    };
    TrainingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-trainings',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\trainings\trainings.html"*/'<!--\n\n  Generated template for the TrainingsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation. navPop\n\n-->\n\n<ion-header>\n\n  <ion-toolbar no-border-top class="ToolBarClass">\n\n    <div class="ToolBarBackButton">\n\n      <button ion-button icon-only clear (click)="goRegister()">\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </div>\n\n    <div class="ToolBarTitle">\n\n      <ion-title>\n\n        <img src="images/head_logo.png" class="headLogo" />\n\n      </ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="Content" align="center">\n\n\n\n  <!-- <div class="backgroundImage">\n\n    <img src="images/trainings_bck.jpg" class="imageclass"/>\n\n  </div> -->\n\n  <div style="width: 100%; margin-top: 15px;" align="center" (click)="gotoAddTraining()">\n\n    <div style="width: 95%" align="center">\n\n      <button ion-button block padding style="font-size: 22px; font-weight: bold">דווח אימון חדש</button>\n\n    </div>\n\n  </div>\n\n\n\n  <div class="trainigInfo">\n\n    <div class="trainingsTitle">\n\n      רשימת האימונים שלי\n\n    </div>\n\n    <!--  -->\n\n    <div class="TraningInfo" *ngFor="let training of TrainingsArray">\n\n      <div class="leftTraningInfo1">\n\n        {{changDateDirection(training.date)}}\n\n      </div>\n\n      <div class="rightTraningInfo1">\n\n        {{ getTrainingType(training.type) }}\n\n\n\n        <!--TrainingsTypes[training.type].title-->\n\n      </div>\n\n      <hr />\n\n    </div>\n\n  </div>\n\n\n\n\n\n</ion-content>\n\n\n\n<!--<ion-footer class="footerClass" (click)="gotoAddTraining()">-->\n\n<!--<img src="images/new_practise_button.jpg" class="imageclass"/>-->\n\n<!--</ion-footer>-->'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\trainings\trainings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_TraingngsService__["a" /* TrainingsSevice */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */]])
    ], TrainingsPage);
    return TrainingsPage;
}());

//# sourceMappingURL=trainings.js.map

/***/ }),

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_meals_meals__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_badge__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_firebase__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_loginService__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_weightService__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_TraingngsService__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















//declare var FCMPlugin;
var MyApp = /** @class */ (function () {
    function MyApp(injector, trainingService, weightService, loginService, firebase, alertCtrl, platform, statusBar, splashScreen, push, Settings, ChatService, events, badge) {
        this.injector = injector;
        this.trainingService = trainingService;
        this.weightService = weightService;
        this.loginService = loginService;
        this.firebase = firebase;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.push = push;
        this.Settings = Settings;
        this.ChatService = ChatService;
        this.events = events;
        this.badge = badge;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] }
        ];
    }
    Object.defineProperty(MyApp.prototype, "navCtrl", {
        get: function () {
            return this.injector.get(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */]);
        },
        enumerable: true,
        configurable: true
    });
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            var d = new Date();
            var dayName = d.getDay();
            var token = '';
            if (window.localStorage.identify != null && window.localStorage.identify != '') {
                if (dayName == 0) {
                    _this.getTraining();
                }
                console.log("Today : ", dayName);
                _this.checkYesterday();
                _this.checkWeight();
                console.log("T2");
                try {
                    _this.loginService.GetToken('GetToken', window.localStorage.push_id).then(function (data) {
                        console.log("DDD : ", data);
                    });
                }
                catch (err) {
                    console.log("ErrToken");
                }
            }
            _this.splashScreen.hide();
            if (_this.platform.is('cordova')) {
                if (_this.platform.is('android')) {
                    _this.initializeFireBaseAndroid();
                }
                if (_this.platform.is('ios')) {
                    _this.initializeFireBaseIos();
                }
            }
            // this.navCtrl.push(ChatPage);
            //this.pushSetup();
            _this.badge.clear();
            _this.platform.registerBackButtonAction(function () {
                if (_this.nav.length() == 1) {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Exit',
                        message: 'Do you want to exit?',
                        buttons: [
                            {
                                text: "OK", handler: function () {
                                    _this.platform.exitApp();
                                }
                            },
                            { text: "Cancel", role: 'cancel' }
                        ]
                    });
                    alert_1.present();
                }
                else
                    _this.nav.pop();
            });
        });
    };
    MyApp.prototype.presentAlert = function (title, message) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['אשר'],
            cssClass: 'alertCustomCss'
        });
        alert.present();
    };
    MyApp.prototype.checkWeight = function () {
        var d = this.getDate();
        if (d != window.localStorage.weightDate) {
            console.log("111 : ", window.localStorage.weightDate);
            // this.weightService.checkWeight('checkWeight').then((data: any) => {
            //     console.log("111")
            //     if(data)
            //     {
            //         if (Number(data) > 0) {
            //             this.presentAlert(    "ירידה במשקל" , "כל הכבוד בשבוע שעבר ירדת  " + Number(data) + " ממשקלך !! כל הכבוד !!")
            //             //alert("כל הכבוד בשבוע שעבר ירדת  " + Number(data) + " ממשקלך !! כל הכבוד !!")
            //         }
            //         else if (Number(data) == 0) {
            //             this.presentAlert( "ירידה במשקל" , " שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
            //             //alert(" שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
            //         }
            //         else if (Number(data) < 0) {
            //             this.presentAlert( "ירידה במשקל" , " שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
            //             //alert(" שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
            //         }
            //
            //         window.localStorage.weightDate = d;
            //
            //     }
            // });
        }
    };
    MyApp.prototype.getDate = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (Number(dd) < 10) {
            dd = '0' + dd.toString();
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '/' + mm + '/' + yyyy;
        return today;
    };
    MyApp.prototype.getTraining = function () {
        var _this = this;
        var d = this.getDate();
        if (d != window.localStorage.trainingDate) {
            this.trainingService.checkWeaklyTraining('checkWeaklyTraining').then(function (data) {
                console.log("checkWeaklyTraining : ", data);
                if (Number(data) > 3) {
                    //alert("כל הכבוד בשבוע שעבר ביצעת  " + Number(data) + " אימונים !! כל הכבוד !!")
                    _this.presentAlert("מספר אימונים בשבוע שעבר", "כל הכבוד בשבוע שעבר ביצעת  " + Number(data) + " אימונים !! כל הכבוד !!");
                }
                else if (Number(data) == 2) {
                    //alert("הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצעו רק  " + Number(data) + 'אימונים');
                    _this.presentAlert("מספר אימונים בשבוע שעבר", "הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצעו רק  " + Number(data) + 'אימונים');
                }
                else if (Number(data) == 1) {
                    //alert("הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצע רק אימון אחד ");
                    _this.presentAlert("מספר אימונים בשבוע שעבר", "הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצע רק אימון אחד ");
                }
                else if (Number(data) == 0) {
                    //alert("הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר לא ביצעת אימונים בכלל ");
                    _this.presentAlert("מספר אימונים בשבוע שעבר", "הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר לא ביצעת אימונים בכלל ");
                }
                window.localStorage.trainingDate = d;
            });
        }
    };
    MyApp.prototype.checkYesterday = function () {
        var _this = this;
        this.loginService.checkYesterday('checkYesterday').then(function (data) {
            console.log("Data1222 : ", data);
            if (data == 0) {
                //alert("שים לב !! אתמול לא דיווחת ארוחות בכלל חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה");
                _this.presentAlert("מספר ארוחות", "שים לב !! אתמול לא דיווחת ארוחות בכלל חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה");
            }
            else if (data < 6 && data > 0) {
                //alert("שים לב !! אתמול דיווחת רק " + data + " ארוחות חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה");
                _this.presentAlert("מספר ארוחות", "שים לב !! אתמול דיווחת רק " + data + " ארוחות חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה");
            }
            else if (data >= 6) {
                //alert("כל הכבוד !! אתמול דיווחת 6 ארוחות אתה בדרך לשינוי");
                _this.presentAlert("מספר ארוחות", "כל הכבוד !! אתמול דיווחת 6 ארוחות אתה בדרך לשינוי");
            }
        });
    };
    MyApp.prototype.initializeFireBaseAndroid = function () {
        var _this = this;
        return this.firebase.getToken()
            .catch(function (error) { return console.log(error); })
            .then(function (token) {
            _this.firebase.subscribe('all').then(function (result) {
                if (result)
                    console.log("Subscribed to all");
                _this.subscribeToPushNotificationEvents();
            });
        });
    };
    MyApp.prototype.initializeFireBaseIos = function () {
        var _this = this;
        return this.firebase.grantPermission()
            .catch(function (error) { return console.error('Error getting permission', error); })
            .then(function () {
            _this.firebase.getToken()
                .catch(function (error) { return console.error('Error getting token', error); })
                .then(function (token) {
                console.log("The token is " + token);
                _this.firebase.subscribe('all').then(function (result) {
                    if (result)
                        console.log("Subscribed to all");
                    _this.subscribeToPushNotificationEvents();
                });
            });
        });
    };
    MyApp.prototype.saveToken = function (token) {
        // Send the token to the server
        // console.log('Sending token to the server...');
        return Promise.resolve(true);
    };
    MyApp.prototype.subscribeToPushNotificationEvents = function () {
        var _this = this;
        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(function (token) {
            _this.Settings.SetUserPush(token);
            _this.saveToken(token);
            _this.loginService.sendToken('GetToken', token).then(function (data) {
                console.log("UserDetails : ", data);
            });
        }, function (error) {
            console.error('Error refreshing token', error);
        });
        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(function (notification) {
            // !notification.tap
            //     ? alert('The user was using the app when the notification arrived...')
            //     : alert('The app was closed when the notification arrived...');
            if (!notification.tap) {
                //alert(JSON.stringify(notification));
                _this.date = new Date();
                _this.hours = _this.date.getHours();
                _this.minutes = _this.date.getMinutes();
                _this.seconds = _this.date.getSeconds();
                if (_this.hours < 10)
                    _this.hours = "0" + _this.hours;
                if (_this.minutes < 10)
                    _this.minutes = "0" + _this.minutes;
                _this.time = _this.hours + ':' + _this.minutes;
                _this.today = new Date();
                _this.dd = _this.today.getDate();
                _this.mm = _this.today.getMonth() + 1; //January is 0!
                _this.yyyy = _this.today.getFullYear();
                if (_this.dd < 10) {
                    _this.dd = '0' + _this.dd;
                }
                if (_this.mm < 10) {
                    _this.mm = '0' + _this.mm;
                }
                _this.today = _this.dd + '/' + _this.mm + '/' + _this.yyyy;
                _this.newdate = _this.today + ' ' + _this.time;
                //alert("Notification " + notification.message );
                _this.Obj = {
                    id: '',
                    uid: 3,
                    title: notification.message,
                    date: _this.newdate,
                    type: notification.type,
                    name: notification.fullname,
                    image: notification.image
                };
                _this.ChatService.pushToArray(_this.Obj);
                _this.events.publish('newchat', "newchat");
            }
            else {
                //alert(JSON.stringify(notification));
                if (window.localStorage.identify && notification.type == '1' || notification.type == '3') {
                    _this.nav.push(__WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__["a" /* ChatPage */]);
                }
            }
            // let notificationAlert = this.alertCtrl.create({
            //     title: notification.title,
            //     message: JSON.stringify(notification),
            //     buttons: ['Ok']
            // });
            // notificationAlert.present();
        }, function (error) {
            console.error('Error getting the notification', error);
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.pushSetup = function () {
        // to initialize push notifications
        var _this = this;
        var options = {
            android: {
                senderID: '1034167244395',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            //alert("Notification222 " + JSON.stringify(notification));
            if (notification.additionalData.foreground) {
                alert("p1");
                _this.date = new Date();
                _this.hours = _this.date.getHours();
                _this.minutes = _this.date.getMinutes();
                _this.seconds = _this.date.getSeconds();
                if (_this.hours < 10)
                    _this.hours = "0" + _this.hours;
                if (_this.minutes < 10)
                    _this.minutes = "0" + _this.minutes;
                _this.time = _this.hours + ':' + _this.minutes;
                _this.today = new Date();
                _this.dd = _this.today.getDate();
                _this.mm = _this.today.getMonth() + 1; //January is 0!
                _this.yyyy = _this.today.getFullYear();
                if (_this.dd < 10) {
                    _this.dd = '0' + _this.dd;
                }
                if (_this.mm < 10) {
                    _this.mm = '0' + _this.mm;
                }
                _this.today = _this.dd + '/' + _this.mm + '/' + _this.yyyy;
                _this.newdate = _this.today + ' ' + _this.time;
                //alert("Notification " + notification.message );
                _this.Obj = {
                    id: '',
                    uid: 3,
                    title: notification.message,
                    date: _this.newdate,
                    type: 1,
                    name: notification.additionalData.fullname,
                    image: notification.image
                };
                _this.ChatService.pushToArray(_this.Obj);
                _this.events.publish('newchat', "newchat");
            }
            else {
                if (window.localStorage.identify) {
                    if (notification.type == 1)
                        _this.nav.push(__WEBPACK_IMPORTED_MODULE_7__pages_chat_chat__["a" /* ChatPage */]);
                    else if (notification.type == 2)
                        _this.nav.push(__WEBPACK_IMPORTED_MODULE_6__pages_meals_meals__["a" /* MealsPage */]);
                }
            }
        });
        pushObject.on('registration').subscribe(function (registration) {
            _this.Settings.SetUserPush(registration.registrationId.toString());
            alert("registration");
        });
        //pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('myNav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<!--<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>-->\n\n<ion-nav #myNav [root]="rootPage" swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injector */], __WEBPACK_IMPORTED_MODULE_15__services_TraingngsService__["a" /* TrainingsSevice */], __WEBPACK_IMPORTED_MODULE_14__services_weightService__["a" /* WeightService */], __WEBPACK_IMPORTED_MODULE_13__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_firebase__["a" /* Firebase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_9__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_10__services_chatService__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_badge__["a" /* Badge */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingModel; });
var TrainingModel = /** @class */ (function () {
    function TrainingModel() {
    }
    return TrainingModel;
}());

//# sourceMappingURL=TrainingModel.js.map

/***/ }),

/***/ 715:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutosizeDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutosizeDirective = /** @class */ (function () {
    function AutosizeDirective(element) {
        var _this = this;
        this.element = element;
        this.onInput = function (textArea) {
            _this.adjust();
        };
        this.adjust = function () {
            var ta = _this.element.nativeElement.querySelector("textarea");
            if (ta !== undefined && ta !== null) {
                ta.style.overflow = "hidden";
                ta.style.height = "auto";
                ta.style.height = ta.scrollHeight + "px";
            }
        };
    }
    AutosizeDirective.prototype.ngOnInit = function () {
        var _this = this;
        var waitThenAdjust = function (trial) {
            if (trial > 10) {
                // Give up.
                return;
            }
            var ta = _this.element.nativeElement.querySelector("textarea");
            if (ta !== undefined && ta !== null) {
                _this.adjust();
            }
            else {
                setTimeout(function () {
                    waitThenAdjust(trial + 1);
                }, 0);
            }
        };
        // Wait for the textarea to properly exist in the DOM, then adjust it.
        waitThenAdjust(1);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])("input", ["$event.target"]),
        __metadata("design:type", Object)
    ], AutosizeDirective.prototype, "onInput", void 0);
    AutosizeDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: "ion-textarea[autosize]" // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], AutosizeDirective);
    return AutosizeDirective;
}());

//# sourceMappingURL=autosize.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MealsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__meals_menu_meals_menu__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_MealsService__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__popup_popup__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_register__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the MealsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MealsPage = /** @class */ (function () {
    function MealsPage(navCtrl, navParams, MealsService, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.MealsService = MealsService;
        this.modalCtrl = modalCtrl;
        this.MealsTitle = ['ארוחה 1', 'ארוחה 2', 'ארוחה 3', 'ארוחה 4', 'ארוחה 5', 'ארוחה 6'];
        this.MealsArray = [];
        this.mealAccept = -1;
        this.loadedData = false;
        this.mealAccept = navParams.get('mealAccept');
    }
    MealsPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad MealsPage');
    };
    MealsPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.MealsService.getMainPageMeals('getMainPageMenu').then(function (data) {
                            _this.MealsArray = data;
                            _this.loadedData = true;
                            if (_this.mealAccept >= 0)
                                _this.acceptMeal(_this.mealAccept);
                            console.log("DT : ", data);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MealsPage.prototype.gotoMenuMeals = function (i) {
        if (this.MealsArray[i].accept == 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__meals_menu_meals_menu__["a" /* MealsMenuPage */], { mealNum: i });
    };
    MealsPage.prototype.getCountArray = function (i) {
        if (this.MealsArray[i]) {
            if (this.MealsArray[i].accept == 1)
                return -1;
            else
                return this.MealsArray[i].products.length;
        }
        else
            return 0;
    };
    MealsPage.prototype.getSelectedProductes = function (index) {
        var str = '';
        if (this.MealsArray[index]) {
            var i = 0;
            var Count = this.MealsArray[index].products.length;
            for (i = 0; i < Count; i++) {
                str += this.MealsArray[index].products[i].name;
                if (i != (this.MealsArray[index].products.length - 1))
                    str += " , ";
            }
            return str;
        }
        else
            return str;
        //return false;
    };
    MealsPage.prototype.acceptMeal = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.MealsArray[index].accept = 1;
                        return [4 /*yield*/, this.MealsService.acceptMeal('acceptMeal1', index).then(function (data) {
                                console.log("acceptMeal : ", data);
                                _this.showPopup(index);
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MealsPage.prototype.showPopup = function (index) {
        var Url;
        if (index == 0 || index == 2 || index == 4)
            Url = 'images/popups/1.png';
        else
            Url = 'images/popups/1.png';
        if (this.MealsArray[0].accept == 1 && this.MealsArray[1].accept == 1 && this.MealsArray[2].accept == 1 && this.MealsArray[3].accept == 1 && this.MealsArray[4].accept == 1 && this.MealsArray[5].accept == 1)
            Url = 'images/popups/3.png';
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__popup_popup__["a" /* PopupPage */], { url: Url }, { cssClass: 'FBPage' });
        modal.present();
        modal.onDidDismiss(function (data) { });
    };
    MealsPage.prototype.gotoRegister = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__register_register__["a" /* RegisterPage */]);
    };
    MealsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-meals',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\meals\meals.html"*/'<!--\n\n  Generated template for the MealsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n    <ion-toolbar no-border-top class="ToolBarClass">\n\n        <div class="ToolBarBackButton">\n\n            <button ion-button icon-only clear (click)="gotoRegister()">\n\n                <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n            </button>\n\n        </div>\n\n        <div class="ToolBarTitle">\n\n            <ion-title>\n\n                <img src="images/head_logo.png" class="headLogo"/>\n\n            </ion-title>\n\n        </div>\n\n    </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <div class="backgroundImage" >\n\n        <img src="images/menu_bck.jpg" class="imageclass"/>\n\n    </div>\n\n\n\n    <div class="scrollTitle">\n\n      <!--  <div class="MealsInfo" (click)="showPopup()">\n\n            <div class="MealsTitle">\n\n                דווח ארוחות\n\n            </div>\n\n        </div> -->\n\n\n\n        <div class="dailyMeals" *ngIf="loadedData">\n\n            <div class="dayMealDiv" *ngFor="let meal of MealsTitle let i=index" align="center">\n\n                <div [ngClass]="{\'dayMeal\' : (getCountArray(i) == 0), \'dayMeal_full\':(getCountArray(i) > 0), \'dayMeal_block\':(getCountArray(i) == -1)}"\n\n                     class="dayMeal">\n\n                    <div class="divMealContent">\n\n                        <div class="dayMealRight"  (click)="gotoMenuMeals(i)">\n\n                            {{meal}}\n\n                            <div class="MealsSubtitle">\n\n                                {{getSelectedProductes(i)}}\n\n                            </div>\n\n                        </div>\n\n                        <div  [ngClass]="{\'dayMealLeft\' : (getCountArray(i) == 0), \'dayMealLeftBlock\':(getCountArray(i) == -1)}">\n\n                            <div *ngIf="getCountArray(i) == 0">\n\n                                <button ion-button="" clear  (click)="gotoMenuMeals(i)">\n\n                                    <ion-icon [ngClass]="{\'dayMealIcon\' : (getCountArray(i) == 0), \'dayMealIcon_white\':(getCountArray(i) > 0)}" name="ios-create-outline"></ion-icon>\n\n                                </button>\n\n                            </div>\n\n\n\n                            <div *ngIf="getCountArray(i) == -1">\n\n                                <button ion-button="" clear >\n\n                                    <ion-icon [ngClass]="{\'dayMealIconBlock\' : (getCountArray(i) == -1)}" name="ios-lock-outline"></ion-icon>\n\n                                </button>\n\n                                <div [ngClass]="{\'editMeal\' : (getCountArray(i) == 0), \'editMeal_white\':(getCountArray(i) > 0)}" ></div>\n\n                            </div>\n\n                        </div>\n\n                        <div class="dayMealLeft">\n\n                            <button ion-button="" clear *ngIf="getCountArray(i) > 0" (click)="acceptMeal(i)">\n\n                                <ion-icon [ngClass]="{\'dayMealIcon\' : (getCountArray(i) == 0), \'dayMealIcon_white\':(getCountArray(i) > 0)}" name="ios-checkmark-circle-outline"></ion-icon>\n\n                            </button>\n\n                            <div *ngIf="getCountArray(i) > 0" class="editMeal_white">אשר ארוחה</div>\n\n                        </div>\n\n                    </div>\n\n                </div>\n\n\n\n            </div>\n\n        </div>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\meals\meals.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_MealsService__["a" /* MealsService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */]])
    ], MealsPage);
    return MealsPage;
}());

//# sourceMappingURL=meals.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__questions_questions__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_weightService__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_TraingngsService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_QuestionService__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_MealsService__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__about_about__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_push__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__customers_customers__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_youtube_video_player__ = __webpack_require__(329);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, sanitizer, push, ChatService, youtube, Settings, _platform, WeightService, TrainingsService, QuestionService, MealsService) {
        this.navCtrl = navCtrl;
        this.sanitizer = sanitizer;
        this.push = push;
        this.ChatService = ChatService;
        this.youtube = youtube;
        this.Settings = Settings;
        this._platform = _platform;
        this.WeightService = WeightService;
        this.TrainingsService = TrainingsService;
        this.QuestionService = QuestionService;
        this.MealsService = MealsService;
        this.newMessage = 'No Message3';
        this.identify = window.localStorage.identify;
        this.screenHeight = screen.height - 100 + 'px';
        this.movie = "";
        this.videoCounter = 0;
        //this.initializeApp();
        //this.pushSetup();
        console.log("Pass : ", this.identify);
        if (window.localStorage.identify)
            this.Settings.UserId = window.localStorage.identify;
    }
    HomePage.prototype.onGoToRegister = function () {
        if (this.identify) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
            //this.navCtrl.push(LoginPage);
        }
        else {
            this.Settings.UserId = this.identify;
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
        }
    };
    HomePage.prototype.onGoToQuestion = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__questions_questions__["a" /* QuestionsPage */]);
    };
    HomePage.prototype.onGoToCustomers = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__customers_customers__["a" /* CustomersPage */]);
    };
    HomePage.prototype.onGoToAboutPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__about_about__["a" /* AboutPage */]);
    };
    HomePage.prototype.updateVideoUrl = function (id) {
        var _this = this;
        if (this.videoCounter == 0) {
            console.log("Update");
            var dangerousVideoUrl = 'https://www.youtube.com/embed/' + id + '?rel=0&showinfo=0';
            setTimeout(function () { _this.videoCounter = 0; }, 300);
            return this.sanitizer.bypassSecurityTrustResourceUrl(dangerousVideoUrl);
        }
        this.videoCounter++;
    };
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Init : ");
        this.QuestionService.getAllQuestions('getQuestions').then(function (data) {
            console.log("Movies : ", data);
            var movies = data.find(function (item) {
                console.log("It : ", item);
                return item['answer1'] == 1;
            });
            _this.movie = movies['title'];
            console.log("Movie : ", _this.movie);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\home\home.html"*/'<!-- <ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Home</ion-title>\n\n  </ion-navbar>\n\n</ion-header>-->\n\n\n\n<ion-content>\n\n  <div>\n\n\n\n    <!-- <iframe class="youTubeVideo"\n\n      [src]="sanitizer.bypassSecurityTrustResourceUrl(\'https://www.youtube.com/embed/\'+movie)" frameborder="0"\n\n      allowfullscreen></iframe> -->\n\n    <!-- <iframe class="youTubeVideo2"\n\n      [src]="sanitizer.bypassSecurityTrustResourceUrl(\'https://www.youtube.com/embed/hY5QSLll65A\')" frameborder="0"\n\n      allowfullscreen width="100%" height="200"></iframe> -->\n\n\n\n    <!-- <iframe *ngIf="movie != \'\'" width="100%" height="230"\n\n      [src]="sanitizer.bypassSecurityTrustResourceUrl(\'https://www.youtube.com/embed/hY5QSLll65A\')" frameborder="0"\n\n      allowfullscreen></iframe> -->\n\n    <iframe class="youTubeVideo" [src]="\'https://www.youtube.com/embed/\'+movie | safe" frameborder="0" allowfullscreen\n\n      width="100%" height="200"></iframe>\n\n    <!-- <iframe width="100%" height="200"\n\n      [src]="sanitizer.bypassSecurityTrustResourceUrl(\'https://www.youtube.com/embed/\'+movie)" frameborder="0"\n\n      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->\n\n\n\n    <div class="mainImage" style="margin-top: 5px;">\n\n      <button no-padding ion-button><img (click)="onGoToAboutPage()" src="images/home/new/15.jpg"\n\n          style="width: 100%;" /></button>\n\n    </div>\n\n\n\n    <div class="mainImage" style="margin-top: 10px;">\n\n      <button no-padding ion-button><img (click)="onGoToRegister()" src="images/home/new/16.jpg"\n\n          style="width: 100%;" /></button>\n\n    </div>\n\n\n\n    <div class="mainImage" style="margin-top: 10px;">\n\n      <button no-padding ion-button><img (click)="onGoToCustomers()" src="images/home/new/17.jpg"\n\n          style="width: 100%;" /></button>\n\n    </div>\n\n\n\n    <!-- <div class="mainImage">\n\n      <button no-padding ion-button><img (click)="onGoToAboutPage()" src="images/main1.jpg"\n\n          style="width: 100%;" /></button>\n\n    </div>\n\n    <div class="mainImage" style="margin-top: 10px;">\n\n      <button no-padding ion-button><img (click)="onGoToQuestion()" src="images/main2.jpg"\n\n          style="width: 100%;" /></button>\n\n    </div>\n\n    <div class="mainImage" style="margin-top: 10px;">\n\n      <button no-padding ion-button><img (click)="onGoToRegister()" src="images/main3.jpg"\n\n          style="width: 100%;" /></button>\n\n    </div> -->\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_11__services_chatService__["a" /* ChatSevice */], __WEBPACK_IMPORTED_MODULE_15__ionic_native_youtube_video_player__["a" /* YoutubeVideoPlayer */], __WEBPACK_IMPORTED_MODULE_10__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Platform */], __WEBPACK_IMPORTED_MODULE_4__services_weightService__["a" /* WeightService */], __WEBPACK_IMPORTED_MODULE_5__services_TraingngsService__["a" /* TrainingsSevice */], __WEBPACK_IMPORTED_MODULE_6__services_QuestionService__["a" /* QuestionService */], __WEBPACK_IMPORTED_MODULE_7__services_MealsService__["a" /* MealsService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var loginService = /** @class */ (function () {
    function loginService(http, Settings, loadingCtrl) {
        this.http = http;
        this.Settings = Settings;
        this.loadingCtrl = loadingCtrl;
        this.Version = '2';
        this.ServerUrl = this.Settings.ServerUrl;
    }
    ;
    loginService.prototype.getUserDetails = function (url, uid) {
        var _this = this;
        var push = '';
        if (window.localStorage.push_id)
            push = window.localStorage.push_id;
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append('uid', uid);
            body.append('push_id', push);
            return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.QuestionsArray = data; }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    loginService.prototype.GetToken = function (url, push) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append('uid', window.localStorage.identify);
            body.append('token', push);
            body.append('ver', '9');
            console.log(window.localStorage.identify, push);
            return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Token"); }).toPromise();
        }
        catch (err) {
            console.log("ER1 ", err);
        }
        finally {
            loading.dismiss();
        }
    };
    loginService.prototype.logOutUser = function () {
        window.localStorage.identify = '';
        window.localStorage.course_id = '';
        window.localStorage.user_image = '';
        window.localStorage.name = '';
        window.localStorage.dayAfter = '';
        this.Settings.UserId = 0;
        this.Settings.CourseId = '';
        this.Settings.FullName = '';
        localStorage.removeItem("identify");
        localStorage.removeItem("course_id");
        localStorage.removeItem("user_image");
        localStorage.removeItem("name");
        localStorage.removeItem("dayAfter");
        window.localStorage.clear();
    };
    loginService.prototype.sendToken = function (url, token) {
        if (window.localStorage.identify) {
            console.log("send : " + url + " :" + token);
            var body = new FormData();
            body.append('uid', window.localStorage.identify);
            body.append('token', token);
            return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
        }
    };
    loginService.prototype.checkYesterday = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Idd : ", window.localStorage.identify); }).toPromise();
    };
    loginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["q" /* LoadingController */]])
    ], loginService);
    return loginService;
}());

;
//# sourceMappingURL=loginService.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_chatService__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, Login, Settings, ChatService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.ChatService = ChatService;
        this.Identity = '';
        console.log('LoginPage');
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.CheckConnect = function () {
        var _this = this;
        this.Login.getUserDetails('getUserDetails_newVersion', this.Identity).then(function (data) {
            if (data != 0)
                _this.setUserSettings(data);
            else
                alert("ת.ז. לא תקינה אנא נסה שנית");
        });
    };
    LoginPage.prototype.setUserSettings = function (data) {
        window.localStorage.identify = data[0].id;
        window.localStorage.course_id = data[0].course_id;
        window.localStorage.user_image = data[0].image;
        window.localStorage.name = data[0].name;
        window.localStorage.dayAfter = data[0].dayAfter;
        this.Settings.CourseId = data[0].course_id;
        this.Settings.FullName = data[0].name;
        //this.ChatService.registerPush("registerPush" , registration.registrationId)
        this.Settings.UserId = data[0].id;
        console.log("UserId : ", data[0].id);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\647\src\pages\login\login.html"*/'\n\n<ion-header>\n\n    <ion-toolbar no-border-top class="ToolBarClass">\n\n        <div class="ToolBarBackButton">\n\n            <button  ion-button icon-only clear navPop>\n\n                <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n            </button>\n\n        </div>\n\n        <div class="ToolBarTitle">\n\n            <ion-title>\n\n                <img src="images/head_logo.png" class="headLogo"  />\n\n            </ion-title>\n\n        </div>\n\n    </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <!-- background Image -->\n\n    <div class="backgroundImage">\n\n        <img src="images/trainings_bck.jpg" class="imageclass"/>\n\n    </div>\n\n\n\n    <!-- QuestionInfo  -->\n\n    <div class="QuestionInfo" align="center">\n\n        <div class="QuestionTitle">\n\n         הרשם למערכת\n\n        </div>\n\n        <div style="width: 100%" align="center">\n\n            <ion-item class="inputIdentify" align="center">\n\n                <ion-input type="tel" name="title" [(ngModel)]="Identity" placeholder="הכנס מספר תעודת זהות"></ion-input>\n\n            </ion-item>\n\n        </div>\n\n        <button ion-button class="IntroButton" (click)="CheckConnect()">המשך</button>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\647\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_5__services_chatService__["a" /* ChatSevice */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[385]);
//# sourceMappingURL=main.js.map